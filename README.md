# H2GConfig

![UCPH](https://img.shields.io/badge/København_Universitet-NBI-blue) ![CERN](https://img.shields.io/badge/CERN-ALICE-critical)

- [H2GConfig](#h2gconfig)
  - [Pre-requisites](#pre-requisites)
  - [Usage](#usage)
      - [Running from source](#running-from-source)
  - [Configuration Files](#configuration-files)
  - [License](#license)
  - [Citation](#citation)

## Pre-requisites

- [PySide6](https://pypi.org/project/PySide6/)
- [Numpy](https://numpy.org)
- [Matplotlib](https://matplotlib.org)
- [loguru](https://pypi.org/project/loguru/)

You can install the required packages by running the following command:

```bash
python3 -m pip install pyside6 numpy matplotlib loguru
```

For python version, python 3.12 is tested. And a minimum version of python 3.7 is required.

## Usage

#### Running from source

To run the application, execute the following command:
    
> **Note:**
> 
> The application should be executed from the root folder of the project.

```bash
python3 H2GCongig.py
```

And you should see the following window:

<img src="doc/starting_window_mac.png" alt="H2GConfig" width="400"/>

After choosing the desired configuration, you'll see the main window:

<img src="doc/main_window_mac.png" alt="H2GConfig" width="700"/>

## Configuration Files

In the `config` folder, you can find the configuration files for the different configurations. Serval configurations are provided as examples.

- `udp_config.json`: Configuration for the UDP communication. Including the IP addresses and ports for the different KCUs and the PC.
- `h2gcroc_1v4_r1.json`: Configuration for the H2GCROC registers. **Users should not modify this file**.
- `Example_config.json`: Example I2C configuration for testing the application.

After the first run, a `config.ini` file will be created in the same folder as the application. This file stores the last configuration used by the user. Deleting this file will reset the configuration to the default one.

## License

This project is licensed under the Creative Commons Attribution 4.0 International License (CC BY 4.0). See the [LICENSE](LICENSE) file for details.

## Citation

If you use this software for research purposes, please cite it as follows: [CITATION](CITATION)