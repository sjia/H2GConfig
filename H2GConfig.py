from PySide6.QtWidgets import QApplication, QMainWindow, QDialog, QVBoxLayout, QHBoxLayout, QLabel, QLineEdit, QPushButton, QTabWidget, QWidget, QSpinBox, QTabBar, QTableWidgetItem, QTableWidget, QGridLayout, QScrollArea, QHeaderView, QCheckBox, QSizePolicy, QFileDialog, QMessageBox
from PySide6.QtCore import Qt
from PySide6.QtGui import QPixmap, QIcon, QColor, QPalette  # <-- new import
import sys, os
import configparser, json
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy as np
from copy import deepcopy
from functools import partial

import H2GConfig_windows
import H2GConfig_components
import H2GConfig_udp



from loguru import logger

log_format = "<level>{time:YYYY-MM-DD HH:mm:ss} | {level:.1s} | {message}</level>"

# Configure the logger with the new format
logger.remove()  # Remove default handler
logger.add(sink=sys.stderr, format=log_format)

software_version = "0.3"

# Insert helper function after imports and before app creation
def applyModernTheme(app):
    # Set Fusion style and a very light palette
    app.setStyle("Fusion")
    palette = app.palette()
    palette.setColor(QPalette.Window, QColor(255, 255, 255))
    palette.setColor(QPalette.WindowText, Qt.black)
    palette.setColor(QPalette.Base, QColor(255, 255, 255))
    palette.setColor(QPalette.AlternateBase, QColor(245, 245, 245))
    palette.setColor(QPalette.ToolTipBase, Qt.black)
    palette.setColor(QPalette.ToolTipText, Qt.black)
    palette.setColor(QPalette.Text, Qt.black)
    palette.setColor(QPalette.Button, QColor(245, 245, 245))
    palette.setColor(QPalette.ButtonText, Qt.black)
    palette.setColor(QPalette.BrightText, Qt.red)
    palette.setColor(QPalette.Link, QColor(0, 102, 204))
    palette.setColor(QPalette.Highlight, QColor(0, 120, 215))
    palette.setColor(QPalette.HighlightedText, Qt.white)
    app.setPalette(palette)

# * === Main Window ===========================================================
class MainWindow(QMainWindow):
    def __init__(self, num_fpgas, num_asics, config_file):
        super().__init__()

        logger.info("Initializing H2GConfig Main Window")

        self.config_file = config_file
        self.cancel_i2c_send = False
    
        # Updated light theme with frames only for containers, buttons, and tabs.
        self.base_widget_background = "background-color: #ffffff;"
        # Remove container border/rounded corners for vbox/hbox and labels.
        self.module_widget_background = "background-color: #ffffff;"  # Previously had border and border-radius
        # Labels use only text formatting, no borders.
        self.module_title_format   = "font-weight: bold; font-family: 'Segoe UI'; font-size: 14px; color: #333333;"
        self.module_content_format = "font-weight: normal; font-family: 'Segoe UI'; font-size: 13px; color: #333333;"
        # Buttons keep their border (frame) styling.
        # Reduce button size: decrease padding and border-radius
        self.common_button_format  = ("QPushButton {font-size: 14px; font-family: 'Segoe UI'; color: #333333; "
                                        "background-color: #ffffff; border: 1px solid #ccc; padding: 2px 4px; border-radius: 1px;} "  
                                        "QPushButton:hover {background-color: #f7f7f7;}"
                                        "QPushButton:disabled {color: #999; background-color: #eee;}")
        # New style for action buttons in Human mode/Calibration tabs (bigger font)
        self.action_button_format  = ("QPushButton {font-size: 16px; font-family: 'Segoe UI'; color: #333333; "
                                        "background-color: #ffffff; border: 1px solid #ccc; padding: 4px 8px; border-radius: 2px;} "  
                                        "QPushButton:hover {background-color: #f7f7f7;}"
                                        "QPushButton:disabled {color: #999; background-color: #eee;}")
        self.common_spinbox_format = "font-size: 12px; font-family: 'Segoe UI'; color: #333333;"
        self.divider_label_format  = "font-size: 12px; font-family: 'Segoe UI'; color: #666666;"
        self.sheet_background = "background-color: #ffffff; font-family: 'Segoe UI';"

        self.num_fpga_tabs = num_fpgas
        self.num_asic_tabs = num_asics

        self.ping_status = ['unknown' for _ in range(self.num_fpga_tabs)]

        self.pc_ip = '127.0.0.1'
        self.pc_port = 11000
        self.current_asic_name = 'unknown'
        self.current_fpga_name = 'unknown'
        self.current_reg_key = ''

        self.using_channel_wise_regs = True

        self.current_config_file_path = ['not set' for _ in range(self.num_fpga_tabs * self.num_asic_tabs)]
        self.link_ip_address_list = ['undefined ip' for _ in range(self.num_fpga_tabs)]
        self.link_port_list = ['undefined port' for _ in range(self.num_fpga_tabs)]
        self.file_sync_states = [0 for _ in range(self.num_fpga_tabs * self.num_asic_tabs)]
        self.i2c_address_dict = {}

        self.udp_config_json_name = 'config/udp_config.json'
        with open(self.udp_config_json_name, 'r') as _udp_config:
            udp_config_json = json.load(_udp_config)
            for key in list(udp_config_json['KCU_UDP'].keys()):
                if "FPGA" in key:
                    key_index = int(key.split(" ")[1])
                    if key_index < len(self.link_ip_address_list):
                        self.link_ip_address_list[key_index] = udp_config_json['KCU_UDP'][key]['ip']
                        self.link_port_list[key_index] = udp_config_json['KCU_UDP'][key]['port']
            self.pc_ip = udp_config_json['PC_UDP']['ip']
            self.pc_port = udp_config_json['PC_UDP']['port']

        self.h2gcroc_reg_channel_wise_group = {
            'CM_', 'CALIB_', 'Channel_', 'HalfWise_'
        }
        self.h2gcroc_reg_top_group = {
            'Top'
        }
        self.h2gcroc_reg_global_analog_group = {
            'Global_Analog_'
        }
        self.h2gcroc_reg_reference_voltage_group = {
            'Reference_Voltage_'
        }
        self.h2gcroc_reg_master_tdc_group = {
            'Master_TDC_'
        }
        self.h2gcroc_reg_digital_half_group = {
            'Digital_Half_'
        }

        default_reg_dict = {}
        reg_name_dict = {}
        self.reginfo = H2GConfig_components.RegInfo()
        self.reginfo.update_reg_signal.connect(self.onRegInfoChanged)
        self.reginfo.update_sync_signal.connect(self.onRegSyncChanged)

        default_reg_file = 'config/h2gcroc_1v4_r1.json'
        with open(default_reg_file, 'r') as f:
            default_reg_json = json.load(f)
            for key in list(default_reg_json.keys()):
                if key.startswith('registers'):
                    target_key = key
                    target_bytearray = bytearray()
                    target_bitnamematrx = []
                    for subkey in list(default_reg_json[target_key][0].keys()):
                        if subkey.startswith('register'):
                            target_bitnamearray = []
                            _byte = default_reg_json[target_key][0][subkey][7]['default_value'] << 7 | default_reg_json[target_key][0][subkey][6]['default_value'] << 6 | default_reg_json[target_key][0][subkey][5]['default_value'] << 5 | default_reg_json[target_key][0][subkey][4]['default_value'] << 4 | default_reg_json[target_key][0][subkey][3]['default_value'] << 3 | default_reg_json[target_key][0][subkey][2]['default_value'] << 2 | default_reg_json[target_key][0][subkey][1]['default_value'] << 1 | default_reg_json[target_key][0][subkey][0]['default_value']
                            target_bitnamearray.append(default_reg_json[target_key][0][subkey][7]['name'])
                            target_bitnamearray.append(default_reg_json[target_key][0][subkey][6]['name'])
                            target_bitnamearray.append(default_reg_json[target_key][0][subkey][5]['name'])
                            target_bitnamearray.append(default_reg_json[target_key][0][subkey][4]['name'])
                            target_bitnamearray.append(default_reg_json[target_key][0][subkey][3]['name'])
                            target_bitnamearray.append(default_reg_json[target_key][0][subkey][2]['name'])
                            target_bitnamearray.append(default_reg_json[target_key][0][subkey][1]['name'])
                            target_bitnamearray.append(default_reg_json[target_key][0][subkey][0]['name'])
                            target_bytearray.append(_byte)
                            target_bitnamematrx.append(target_bitnamearray)
                    default_reg_dict[target_key] = target_bytearray
                    reg_name_dict[target_key] = target_bitnamematrx

            for key in list(default_reg_json['I2C_address'].keys()):
                self.i2c_address_dict[key] = default_reg_json['I2C_address'][key]

        
            for _fpga_index in range(self.num_fpga_tabs):
                fpga_reg_dicts = []
                fpga_reg_names = []
                for _asic_index in range(self.num_asic_tabs):
                    asic_reg_dict = {}
                    asic_reg_name = {}

                    # * Set default values
                    for key in list(default_reg_json['I2C_address'].keys()):
                        # reg_key = H2GConfig_components.regNameTranslation2JSON(key)
                        reg_key = key
                        found_group = False
                        for _pattern in self.h2gcroc_reg_channel_wise_group:
                            if _pattern in reg_key and not found_group:
                                asic_reg_dict[reg_key] = default_reg_dict['registers_channel_wise'].copy()
                                asic_reg_name[reg_key] = reg_name_dict['registers_channel_wise'].copy()
                                found_group = True
                        for _pattern in self.h2gcroc_reg_top_group:
                            if _pattern in reg_key and not found_group:
                                asic_reg_dict[reg_key] = default_reg_dict['registers_top'].copy()
                                asic_reg_name[reg_key] = reg_name_dict['registers_top'].copy()
                                found_group = True
                        for _pattern in self.h2gcroc_reg_global_analog_group:
                            if _pattern in reg_key and not found_group:
                                asic_reg_dict[reg_key] = default_reg_dict['registers_global_analog'].copy()
                                asic_reg_name[reg_key] = reg_name_dict['registers_global_analog'].copy()
                                found_group = True
                        for _pattern in self.h2gcroc_reg_reference_voltage_group:
                            if _pattern in reg_key and not found_group:
                                asic_reg_dict[reg_key] = default_reg_dict['registers_reference_voltage'].copy()
                                asic_reg_name[reg_key] = reg_name_dict['registers_reference_voltage'].copy()
                                found_group = True
                        for _pattern in self.h2gcroc_reg_master_tdc_group:
                            if _pattern in reg_key and not found_group:
                                asic_reg_dict[reg_key] = default_reg_dict['registers_master_tdc'].copy()
                                asic_reg_name[reg_key] = reg_name_dict['registers_master_tdc'].copy()
                                found_group = True
                        for _pattern in self.h2gcroc_reg_digital_half_group:
                            if _pattern in reg_key and not found_group:
                                asic_reg_dict[reg_key] = default_reg_dict['registers_digital_half'].copy()
                                asic_reg_name[reg_key] = reg_name_dict['registers_digital_half'].copy()
                                found_group = True
                        if not found_group:
                            print(f"Key {reg_key} not found in any group")
                    fpga_reg_dicts.append(deepcopy(asic_reg_dict))
                    fpga_reg_names.append(deepcopy(asic_reg_name))
                self.reginfo.all_reg_dicts.append(deepcopy(fpga_reg_dicts))
                self.reginfo.all_reg_names.append(deepcopy(fpga_reg_names))
        self.initUI()
        self.loadIniConfig()

    def initUI(self):
        logger.info("Initializing H2GConfig UI")

        self._human_mode_dplots_properties_dict = {
            "inputdac": {"title": "Input DAC",      "xlabel": "Channel", "ylabel": "Input DAC Value",   "ylim": (-1, 64), "color": "blue"},
            "trim_toa": {"title": "Trim ToA DAC",   "xlabel": "Channel", "ylabel": "Trim ToA Value",    "ylim": (-1, 64), "color": "red"},
            "trim_tot": {"title": "Trim ToT DAC",   "xlabel": "Channel", "ylabel": "Trim ToT Value",    "ylim": (-1, 64), "color": "green"},
            "trim_inv": {"title": "Trim Pede DAC",  "xlabel": "Channel", "ylabel": "Trim DAC Value",    "ylim": (-1, 64), "color": "purple"},
        }

        # Updated light tab styles with a smaller header font and consistent font
        tab_style_sheet = """
        QTabWidget::pane { 
            border: 1px solid #ccc;
            background: #ffffff;
        }
        QTabBar::tab {
            background: #f8f8f8;
            color: #333333;
            font-family: 'Segoe UI';
            font-size: 10px;
            border: 1px solid #ccc;
            padding: 6px 12px;
            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
        }
        QTabBar::tab:selected { 
            background: #ffffff; 
            border-bottom: 1px solid #ffffff;
        }
        QTabBar::tab:hover { 
            background: #f0f0f0;
        }
        """
        self.central_widget = QWidget()
        self.central_widget.setStyleSheet(self.base_widget_background)
        self.setCentralWidget(self.central_widget)
        self.central_layout = QHBoxLayout()
        self.central_widget.setLayout(self.central_layout)

        self.tabs = QTabWidget()
        self.tabs.setStyleSheet(tab_style_sheet)
        self.central_layout.addWidget(self.tabs, stretch=3)

        self.link_sidebar = QWidget()
        self.link_sidebar.setStyleSheet(self.module_widget_background + "font-family: 'DejaVu Sans Mono';" + "font-size: 12px;")
        self.link_sidebar_layout = QVBoxLayout()
        self.link_sidebar.setLayout(self.link_sidebar_layout)
        self.link_sidebar.setMaximumWidth(250)
        self.central_layout.addWidget(self.link_sidebar, stretch=1)

        # * add link sidebar components
        self.link_sidebar_FPGA_label = QLabel("unknown")
        self.link_sidebar_FPGA_label_title = QLabel("FPGA selection:")
        self.link_sidebar_ASIC_label = QLabel("unknown", alignment=Qt.AlignLeft)
        self.link_sidebar_ASIC_label_title = QLabel("ASIC selection:", alignment=Qt.AlignLeft)
        self.link_sidebar_FPGA_label.setStyleSheet(self.module_content_format)
        self.link_sidebar_FPGA_label_title.setStyleSheet(self.module_content_format)
        self.link_sidebar_ASIC_label.setStyleSheet(self.module_content_format)
        self.link_sidebar_ASIC_label_title.setStyleSheet(self.module_content_format)

        link_sidebar_current_config_divider = QLabel("-- Current Config --")
        link_sidebar_current_config_divider.setStyleSheet(self.divider_label_format)
        link_sidebar_current_config_divider.setFixedHeight(20)
        self.link_sidebar_layout.addWidget(link_sidebar_current_config_divider, alignment=Qt.AlignCenter)


        link_sidebar_FPGA_info_widget = QWidget()
        link_sidebar_FPGA_info_layout = QHBoxLayout()
        link_sidebar_FPGA_info_layout.setContentsMargins(0, 0, 0, 0)
        link_sidebar_FPGA_info_widget.setLayout(link_sidebar_FPGA_info_layout)
        link_sidebar_FPGA_info_layout.addWidget(self.link_sidebar_FPGA_label_title)
        link_sidebar_FPGA_info_layout.addWidget(self.link_sidebar_FPGA_label)

        self.link_sidebar_layout.addWidget(link_sidebar_FPGA_info_widget)

        link_sidebar_ASIC_info_widget = QWidget()
        link_sidebar_ASIC_info_layout = QHBoxLayout()
        link_sidebar_ASIC_info_layout.setContentsMargins(0, 0, 0, 0)
        link_sidebar_ASIC_info_widget.setLayout(link_sidebar_ASIC_info_layout)
        link_sidebar_ASIC_info_layout.addWidget(self.link_sidebar_ASIC_label_title,alignment=Qt.AlignLeft)
        link_sidebar_ASIC_info_layout.addWidget(self.link_sidebar_ASIC_label, alignment=Qt.AlignLeft)

        self.link_sidebar_layout.addWidget(link_sidebar_ASIC_info_widget)

        link_sidebar_reg_sel_divider = QLabel("-- Register Selection --")
        link_sidebar_reg_sel_divider.setStyleSheet(self.divider_label_format)
        link_sidebar_reg_sel_divider.setFixedHeight(20)
        self.link_sidebar_layout.addWidget(link_sidebar_reg_sel_divider, alignment=Qt.AlignCenter)


        self.link_sidebar_halfwise_reg_checkbox = QCheckBox("Use half-wise    registers")
        self.link_sidebar_channelwise_reg_checkbox = QCheckBox("Use channel-wise registers")
        self.link_sidebar_halfwise_reg_checkbox.setStyleSheet(self.module_content_format)
        self.link_sidebar_channelwise_reg_checkbox.setStyleSheet(self.module_content_format)
        self.link_sidebar_halfwise_reg_checkbox.setChecked(not self.using_channel_wise_regs)
        self.link_sidebar_channelwise_reg_checkbox.setChecked(self.using_channel_wise_regs)
        self.link_sidebar_halfwise_reg_checkbox.stateChanged.connect(self.onHalfWiseCheckboxChecked)
        self.link_sidebar_channelwise_reg_checkbox.stateChanged.connect(self.onChannelWiseCheckboxChecked)
        self.link_sidebar_layout.addWidget(self.link_sidebar_halfwise_reg_checkbox)
        self.link_sidebar_layout.addWidget(self.link_sidebar_channelwise_reg_checkbox)

        # * add config file info
        link_sidebar_config_file_divider = QLabel("-- Config File Info --")
        link_sidebar_config_file_divider.setStyleSheet(self.divider_label_format)
        link_sidebar_config_file_divider.setFixedHeight(20)
        self.link_sidebar_layout.addWidget(link_sidebar_config_file_divider, alignment=Qt.AlignCenter)

        self.link_sidebar_config_file_label = QLabel("not set")
        self.link_sidebar_config_file_label.setWordWrap(True)
        self.link_sidebar_config_file_label.setStyleSheet(self.module_content_format)
        self.link_sidebar_layout.addWidget(self.link_sidebar_config_file_label)

        self.link_sidebar_sync_led = H2GConfig_components.LedIndicator()
        self.link_sidebar_sync_led.setFixedHeight(20)
        self.link_sidebar_layout.addWidget(self.link_sidebar_sync_led)

        link_sidebar_config_buttons_widget = QWidget()
        link_sidebar_config_buttons_layout = QHBoxLayout()
        link_sidebar_config_buttons_layout.setContentsMargins(0, 0, 0, 0)
        link_sidebar_config_buttons_widget.setLayout(link_sidebar_config_buttons_layout)

        self.link_sidebar_config_load_button = QPushButton("Load")
        self.link_sidebar_config_load_button.setStyleSheet(self.common_button_format)
        self.link_sidebar_config_save_button = QPushButton("Save")
        self.link_sidebar_config_save_button.setStyleSheet(self.common_button_format)
        self.link_sidebar_config_save_button.setDisabled(True)
        self.link_sidebar_config_save_as_button = QPushButton("Save As")
        self.link_sidebar_config_save_as_button.setStyleSheet(self.common_button_format)
        self.link_sidebar_config_load_button.setFixedHeight(30)
        self.link_sidebar_config_save_button.setFixedHeight(30)
        self.link_sidebar_config_save_as_button.setFixedHeight(30)

        # * connect buttons to open file dialog
        self.link_sidebar_config_load_button.clicked.connect(lambda: H2GConfig_components.onLoadConfigButtonClicked(self, self.current_fpga_name, self.current_asic_name, self.reginfo))

        self.link_sidebar_config_save_button.clicked.connect(lambda: H2GConfig_components.onSaveConfigButtonClicked(self, self.current_fpga_name, self.current_asic_name, self.reginfo))

        self.link_sidebar_config_save_as_button.clicked.connect(lambda: H2GConfig_components.onSaveAsConfigButtonClicked(self, self.current_fpga_name, self.current_asic_name))

        link_sidebar_config_buttons_layout.addWidget(self.link_sidebar_config_load_button, stretch=1)
        link_sidebar_config_buttons_layout.addWidget(self.link_sidebar_config_save_button, stretch=1)
        link_sidebar_config_buttons_layout.addWidget(self.link_sidebar_config_save_as_button, stretch=1)

        self.link_sidebar_layout.addWidget(link_sidebar_config_buttons_widget)

        self.link_sidebar_layout.addStretch(1)

        link_sidebar_link_info_divider = QLabel("-- Link Info --")
        link_sidebar_link_info_divider.setStyleSheet(self.divider_label_format)
        link_sidebar_link_info_divider.setFixedHeight(20)
        self.link_sidebar_layout.addWidget(link_sidebar_link_info_divider, alignment=Qt.AlignCenter)

        link_sidebar_link_ip_widget = QWidget()
        link_sidebar_link_ip_layout = QHBoxLayout()
        link_sidebar_link_ip_layout.setContentsMargins(0,0,0,0)
        link_sidebar_link_ip_widget.setLayout(link_sidebar_link_ip_layout)
        self.link_sidebar_layout.addWidget(link_sidebar_link_ip_widget)

        self.link_sidebar_link_ip_label = QLabel("undefined ip")
        self.link_sidebar_link_ip_label.setStyleSheet(self.module_content_format)
        self.link_sidebar_link_ip_title_label = QLabel("IP Address:")
        self.link_sidebar_link_ip_title_label.setStyleSheet(self.module_content_format)

        link_sidebar_link_ip_layout.addWidget(self.link_sidebar_link_ip_title_label)
        link_sidebar_link_ip_layout.addWidget(self.link_sidebar_link_ip_label)

        link_sidebar_link_port_widget = QWidget()
        link_sidebar_link_port_layout = QHBoxLayout()
        link_sidebar_link_port_layout.setContentsMargins(0,0,0,0)
        link_sidebar_link_port_widget.setLayout(link_sidebar_link_port_layout)
        self.link_sidebar_layout.addWidget(link_sidebar_link_port_widget)

        self.link_sidebar_link_port_label = QLabel("undefined port")
        self.link_sidebar_link_port_label.setStyleSheet(self.module_content_format)
        self.link_sidebar_link_port_title_label = QLabel("Port:")
        self.link_sidebar_link_port_title_label.setStyleSheet(self.module_content_format)

        link_sidebar_link_port_layout.addWidget(self.link_sidebar_link_port_title_label)
        link_sidebar_link_port_layout.addWidget(self.link_sidebar_link_port_label)

        self.link_sidebar_readback_button = QPushButton('Readback')
        self.link_sidebar_readback_button.setStyleSheet(self.common_button_format)
        self.link_sidebar_readback_button.setFixedHeight(25)
        self.link_sidebar_layout.addWidget(self.link_sidebar_readback_button)
        self.link_sidebar_readback_button.clicked.connect(lambda: H2GConfig_udp.onReadBack(self, self.current_fpga_name, self.current_asic_name, True))

        # * ping button
        self.link_sidebar_ping_button = QPushButton('Ping FPGA')
        self.link_sidebar_ping_button.setStyleSheet(self.common_button_format)
        self.link_sidebar_ping_button.setFixedHeight(25)
        self.link_sidebar_layout.addWidget(self.link_sidebar_ping_button)
        self.link_sidebar_ping_button.clicked.connect(self.onPingButtonClicked)

        # * send config buttons
        self.send_config_button = QPushButton('Send Current ASIC Config')
        self.send_all_button = QPushButton('Send All Configs')
        self.send_config_button.setStyleSheet(self.common_button_format)
        self.send_all_button.setStyleSheet(self.common_button_format)
        self.send_config_button.setFixedHeight(40)
        self.send_all_button.setFixedHeight(40)

        self.link_sidebar_layout.addWidget(self.send_config_button)
        self.link_sidebar_layout.addWidget(self.send_all_button)

        self.send_config_button.clicked.connect(lambda: H2GConfig_udp.onSendConfig(self, self.current_fpga_name, self.current_asic_name, True))

        self.send_all_button.clicked.connect(lambda: H2GConfig_udp.onSendAllConfig(self, True))

        # self.tabs.setStyleSheet(self.module_widget_background + "font-family: 'DejaVu Sans Mono';" + "font-size: 12px;")
        reg_sel_buttons = []
        self.asic_tab_layouts = []
        self.draggable_plots = []
        self.pede_canvas = []
        self.pede_axes = []
        self.pede_plots = []

        for _fpga_index in range(self.num_fpga_tabs):
            fpga_tab = QWidget()
            self.draggable_plots.append([])
            self.pede_axes.append([])
            self.pede_canvas.append([])
            self.pede_plots.append([])

            fpga_tab.setStyleSheet(tab_style_sheet)
            fpga_tab_layout = QVBoxLayout()
            fpga_tab_layout.setContentsMargins(0, 0, 0, 0)  # Reduce content margins
            fpga_tab_layout.setSpacing(0)
            fpga_tab.setLayout(fpga_tab_layout)
            self.tabs.addTab(fpga_tab, f"FPGA {_fpga_index}")
            fpga_tab_subtabs = QTabWidget()
            fpga_tab_subtabs.setStyleSheet(tab_style_sheet)
            fpga_reg_sel_buttons = []
            fpga_asic_tab_layouts = []
            for _asic_index in range(self.num_asic_tabs):
                self.draggable_plots[_fpga_index].append([])
                self.pede_axes[_fpga_index].append([])
                self.pede_canvas[_fpga_index].append([])
                self.pede_plots[_fpga_index].append([])
                
                asic_tab = QTabWidget()
                asic_tab.setStyleSheet(tab_style_sheet)
                asic_tab_layout = QVBoxLayout()
                asic_tab_layout.setContentsMargins(0, 0, 0, 0)  # Reduce content margins
                asic_tab_layout.setSpacing(0)
                asic_tab.setLayout(asic_tab_layout)

                asic_tab.setTabPosition(QTabWidget.West)
                asic_tab_human_mode     = QWidget()
                asic_tab_human_layout   = QVBoxLayout()
                asic_tab_human_mode.setLayout(asic_tab_human_layout)
                asic_tab_robot_mode     = QWidget()
                asic_tab_robot_layout   = QHBoxLayout()
                asic_tab_robot_mode.setLayout(asic_tab_robot_layout)
                asic_tab_calib = QWidget()
                asic_tab_calib_layout = QVBoxLayout()
                asic_tab_calib.setLayout(asic_tab_calib_layout)
                
                asic_tab.addTab(asic_tab_robot_mode, "Robot Mode")

                # * - add picture to human mode -
                asic_tab_human_scroll_area = QScrollArea()
                asic_tab_human_scroll_area.setWidgetResizable(True)

                asic_tab_human_scroll_area.setWidget(asic_tab_human_mode) 
                asic_tab.addTab(asic_tab_human_scroll_area, "Human Mode")

                asic_tab.addTab(asic_tab_calib, "Calibration")
                
                # connect human mode tab selection to update current asic
                # print(f"Connecting tab change signal for fpga {_fpga_index} asic {_asic_index}")
                asic_tab.currentChanged.connect(lambda index,  fpga_index=_fpga_index, asic_list=_asic_index: H2GConfig_components.onModeChangeButtonClicked(self, index, fpga_index, asic_list))

                H2GConfig_components.setupHumanMode(self, asic_tab_human_layout, asic_tab_human_mode, _fpga_index, _asic_index)

                asic_tab_human_layout.addStretch(1)

                # * --- set asic tab layout ---
                asic_tab_reg_widget = QWidget()
                asic_tab_reg_widget.setStyleSheet(self.module_widget_background + "font-family: 'DejaVu Sans Mono';" + "font-size: 12px;")
                asic_tab_reg_widget.setFixedWidth(200)
                asic_tab_reg_layout = QVBoxLayout()
                asic_tab_reg_widget.setLayout(asic_tab_reg_layout)

                asic_tab_reg_widget_scroll_area = QScrollArea()
                asic_tab_reg_widget_scroll_area.setWidget(asic_tab_reg_widget)
                asic_tab_reg_widget_scroll_area.setWidgetResizable(True)
                asic_tab_reg_widget_scroll_area.setFixedWidth(220)
                asic_tab_robot_layout.addWidget(asic_tab_reg_widget_scroll_area, stretch=1)
                
                asic_reg_sel_buttons = {}

                # * - add buttons for top registers -
                H2GConfig_components.createButtonGroup(self, _fpga_index, _asic_index, '-- Top Register --', 1, asic_tab_reg_layout, asic_reg_sel_buttons, self.h2gcroc_reg_top_group)

                # * - add buttons for global analog registers -
                H2GConfig_components.createButtonGroup(self, _fpga_index, _asic_index, '-- Global Analog Registers --', 1, asic_tab_reg_layout, asic_reg_sel_buttons, self.h2gcroc_reg_global_analog_group)
                
                # * - add buttons for reference voltage registers -
                H2GConfig_components.createButtonGroup(self, _fpga_index, _asic_index, '-- Reference Voltage Registers --', 1, asic_tab_reg_layout, asic_reg_sel_buttons, self.h2gcroc_reg_reference_voltage_group)

                # * - add buttons for master tdc registers -
                H2GConfig_components.createButtonGroup(self, _fpga_index, _asic_index, '-- Master TDC Registers --', 1, asic_tab_reg_layout, asic_reg_sel_buttons, self.h2gcroc_reg_master_tdc_group)

                # * - add buttons for digital half registers -
                H2GConfig_components.createButtonGroup(self, _fpga_index, _asic_index, '-- Digital Half Registers --', 1, asic_tab_reg_layout, asic_reg_sel_buttons, self.h2gcroc_reg_digital_half_group)

                # * - add buttons for channel wise registers -
                H2GConfig_components.createButtonGroup(self, _fpga_index, _asic_index, '-- Channel Wise Registers --', 2, asic_tab_reg_layout, asic_reg_sel_buttons, self.h2gcroc_reg_channel_wise_group)

                fpga_tab_subtabs.addTab(asic_tab, f"ASIC {_asic_index}")
                fpga_reg_sel_buttons.append(asic_reg_sel_buttons)

                asic_tab_sheet_widget = QWidget()
                asic_tab_sheet_layout = QVBoxLayout()
                asic_tab_sheet_widget.setLayout(asic_tab_sheet_layout)
                asic_tab_robot_layout.addWidget(asic_tab_sheet_widget, stretch=3)
                asic_tab_sheet_default_label = QLabel("Select a register to view its content")
                asic_tab_sheet_default_label.setStyleSheet(self.divider_label_format)
                asic_tab_sheet_default_label.setFixedHeight(20)
                asic_tab_sheet_layout.addWidget(asic_tab_sheet_default_label, alignment=Qt.AlignCenter)

                fpga_asic_tab_layouts.append(asic_tab_sheet_layout)

                # * - add calibration tab -
                calib_pede_buttons_hbox = QHBoxLayout()
                calib_pede_buttons_hbox.setContentsMargins(0, 0, 0, 0)
                calib_pede_buttons_widget = QWidget()
                calib_pede_buttons_widget.setLayout(calib_pede_buttons_hbox)

                calib_getpede_button = QPushButton("Get Pedestal")
                calib_getpede_button.setStyleSheet(self.action_button_format)
                calib_getpede_button.setFixedHeight(30)

                logger.debug(f"Connecting get pedestal button for fpga {_fpga_index} asic {_asic_index}")
                # calib_getpede_button.clicked.connect(lambda fpga_index=_fpga_index, asic_list=_asic_index: H2GConfig_components.onGetPedestal(self, fpga_index, asic_list))
                calib_getpede_button.clicked.connect(partial(H2GConfig_components.onGetPedestal, self, _fpga_index, _asic_index))

                calib_savepede_button = QPushButton("Save Pedestal")
                calib_savepede_button.setStyleSheet(self.common_button_format)
                calib_savepede_button.setFixedHeight(30)

                calib_pede_buttons_hbox.addWidget(calib_getpede_button)
                # calib_pede_buttons_hbox.addWidget(calib_savepede_button)

                asic_tab_calib_layout.addWidget(calib_pede_buttons_widget)

                H2GConfig_components.setupPedePlot(self, asic_tab_calib_layout, _fpga_index, _asic_index)

                asic_tab_calib_layout.addStretch(1)

            
            fpga_tab_subtabs.currentChanged.connect(lambda index, fpga_index=_fpga_index, asic_list=fpga_tab_subtabs: self.update_current_asic(index, fpga_index, asic_list))
            reg_sel_buttons.append(fpga_reg_sel_buttons)
            fpga_tab_layout.addWidget(fpga_tab_subtabs)
            self.asic_tab_layouts.append(fpga_asic_tab_layouts)
        self.tabs.currentChanged.connect(self.update_current_fpga)
        
        for _fpga_index in range(self.num_fpga_tabs):
            for _asic_index in range(self.num_asic_tabs):
                for key in list(reg_sel_buttons[_fpga_index][_asic_index].keys()):
                    reg_sel_buttons[_fpga_index][_asic_index][key].clicked.connect(lambda ch, parent=self.asic_tab_layouts[_fpga_index][_asic_index] ,fpga_index=_fpga_index, asic_index=_asic_index, reg_key=key: H2GConfig_components.onRegButtonClicked(self, parent, fpga_index, asic_index, reg_key))

        self.setWindowTitle("H2GConfig")
        self.resize(900, 600)

        logger.info("H2GConfig UI Initialized")

    def update_current_fpga(self, index):
        _fpga_tab = self.tabs.widget(index)
        _fpga_tab_layout = _fpga_tab.layout()
        _fpga_tab_subtabs = _fpga_tab_layout.itemAt(0).widget()
        _asic_index = _fpga_tab_subtabs.currentIndex()
        self.current_fpga_name = self.tabs.tabText(index)
        self.current_asic_name = _fpga_tab_subtabs.tabText(_asic_index)
        self.link_sidebar_ASIC_label.setText(self.current_asic_name)
        self.link_sidebar_FPGA_label.setText(self.current_fpga_name)
        self.link_sidebar_config_file_label.setText(self.current_config_file_path[index * self.num_asic_tabs + _asic_index])
        self.link_sidebar_link_ip_label.setText(self.link_ip_address_list[index])
        self.link_sidebar_link_port_label.setText(str(self.link_port_list[index]))
        self.link_sidebar_sync_led.change_state(self.file_sync_states[index*self.num_asic_tabs + _asic_index])
        self.link_sidebar_config_save_button.setEnabled(self.file_sync_states[index*self.num_asic_tabs + _asic_index] == 2)
        if self.ping_status[index] == 'success':
            self.link_sidebar_ping_button.setStyleSheet(self.common_button_format + "QPushButton {background-color: #a0ffa0;}")
        elif self.ping_status[index] == 'failed':
            self.link_sidebar_ping_button.setStyleSheet(self.common_button_format + "QPushButton {background-color: #ffa0a0;}")
        else:
            self.link_sidebar_ping_button.setStyleSheet(self.common_button_format + "QPushButton {background-color: #f0f0f0;}")


    def update_current_asic(self, index, fpga_index, asic_tab_list):
        self.current_asic_name = asic_tab_list.tabText(index)
        if self.current_fpga_name != self.tabs.tabText(fpga_index):
            if self.current_fpga_name == "unknown":
                self.current_fpga_name = self.tabs.tabText(fpga_index)
            else:
                print("Error: FPGA tab and ASIC tab do not match")
                return 
        self.link_sidebar_ASIC_label.setText(self.current_asic_name)
        self.link_sidebar_FPGA_label.setText(self.current_fpga_name)
        self.link_sidebar_config_file_label.setText(self.current_config_file_path[fpga_index * self.num_asic_tabs + index])
        self.link_sidebar_link_ip_label.setText(self.link_ip_address_list[fpga_index])
        self.link_sidebar_link_port_label.setText(str(self.link_port_list[fpga_index]))
        self.link_sidebar_sync_led.change_state(self.file_sync_states[fpga_index*self.num_asic_tabs + index])
        self.link_sidebar_config_save_button.setEnabled(self.file_sync_states[fpga_index*self.num_asic_tabs + index] == 2)

    def onRegInfoChanged(self, fpga_index, asic_index, row_number):
        if int(self.current_fpga_name.split(" ")[-1]) == fpga_index and int(self.current_asic_name.split(" ")[-1]) == asic_index:
            if self.current_reg_key != '':
                # H2GConfig_components.onRegButtonClicked(self, self.asic_tab_layouts[fpga_index][asic_index], fpga_index, asic_index, self.current_reg_key)
                logger.debug(f"row number: {row_number}")
                H2GConfig_components.onRegRefresh(self, self.asic_tab_layouts[fpga_index][asic_index], fpga_index, asic_index, self.current_reg_key, row_number*2+1)

    def onPingButtonClicked(self):
        if sys.platform == 'linux' or sys.platform == 'darwin':
            import subprocess
            _ping_res = subprocess.run(["ping", "-s", "32", "-c", "5", self.link_ip_address_list[int(self.current_fpga_name.split(" ")[-1])]] , stdout=subprocess.PIPE, text=True)
            _success_count = _ping_res.stdout.count("bytes from")
            # print(f"Success count: {_success_count}")
            if _success_count > 0:
                QMessageBox.information(self, "Ping Result", f"Successfully pinged FPGA {_success_count} times")
                self.ping_status[int(self.current_fpga_name.split(" ")[-1])] = 'success'
                self.link_sidebar_ping_button.setStyleSheet(self.common_button_format + "QPushButton {background-color: #a0ffa0;}")
            else:
                QMessageBox.warning(self, "Ping Result", f"Failed to ping FPGA")
                self.ping_status[int(self.current_fpga_name.split(" ")[-1])] = 'failed'
                self.link_sidebar_ping_button.setStyleSheet(self.common_button_format + "QPushButton {background-color: #ffa0a0;}")
        else:
            QMessageBox.warning(self, "Ping Result", f"Pinging is only supported on Linux and MacOS")

    def onRegSyncChanged(self, fpga_index, asic_index, state):
        self.file_sync_states[fpga_index*self.num_asic_tabs + asic_index] = state
        if fpga_index == int(self.current_fpga_name.split(" ")[-1]):
            if asic_index == int(self.current_asic_name.split(" ")[-1]):
                self.link_sidebar_sync_led.change_state(state)
                if state == 2:
                    self.link_sidebar_config_save_button.setEnabled(True)
                else:
                    self.link_sidebar_config_save_button.setDisabled(True)

    def onHalfWiseCheckboxChecked(self, state):
        if state == 2:
            self.using_channel_wise_regs = False
            self.link_sidebar_channelwise_reg_checkbox.setChecked(False)

    def onChannelWiseCheckboxChecked(self, state):
        if state == 2:
            self.using_channel_wise_regs = True
            self.link_sidebar_halfwise_reg_checkbox.setChecked(False)

    def closeEvent(self, event):
        self.saveIniConfig()
        super().closeEvent(event)

    def loadIniConfig(self):
        config = configparser.ConfigParser()
        config.read(self.config_file)

        last_fpga_index = config.getint('Initial Settings', 'last_fpga_index', fallback=0)
        last_asic_index = config.getint('Initial Settings', 'last_asic_index', fallback=0)
        last_using_channel_wise_regs = config.getboolean('Initial Settings', 'using_channel_wise_regs', fallback=True)

        self.using_channel_wise_regs = last_using_channel_wise_regs
        self.link_sidebar_channelwise_reg_checkbox.setChecked(self.using_channel_wise_regs)
        self.link_sidebar_halfwise_reg_checkbox.setChecked(not self.using_channel_wise_regs)

        if last_fpga_index >= self.num_fpga_tabs:
            last_fpga_index = 0
        if last_asic_index >= self.num_asic_tabs:
            last_asic_index = 0

        self.tabs.setCurrentIndex(last_fpga_index)
        self.update_current_fpga(last_fpga_index)

        _fpga_tab = self.tabs.widget(last_fpga_index)
        _fpga_tab_layout = _fpga_tab.layout()
        _fpga_tab_subtabs = _fpga_tab_layout.itemAt(0).widget()
        _fpga_tab_subtabs.setCurrentIndex(last_asic_index)
        self.update_current_asic(last_asic_index, last_fpga_index, _fpga_tab_subtabs)

        # * load all config file paths
        if config.has_section('Config Files'):
            for _fpga_index in range(self.num_fpga_tabs):
                for _asic_index in range(self.num_asic_tabs):
                    if config.has_option('Config Files', f'fpga{_fpga_index}_asic{_asic_index}'):
                        # check if file exists
                        if os.path.isfile(config.get('Config Files', f'fpga{_fpga_index}_asic{_asic_index}')):
                            H2GConfig_components.loadConfigFile(self, _fpga_index, _asic_index, config.get('Config Files', f'fpga{_fpga_index}_asic{_asic_index}'))
                        else:
                            logger.warning(f"Config file {config.get('Config Files', f'fpga{_fpga_index}_asic{_asic_index}')} not found")

    def saveIniConfig(self):
        config = configparser.ConfigParser()
        config.read(self.config_file)
        if not config.has_section('Initial Settings'):
            config.add_section('Initial Settings')
        config.set('Initial Settings', 'last_fpga_index', self.current_fpga_name.split(" ")[-1])
        config.set('Initial Settings', 'last_asic_index', self.current_asic_name.split(" ")[-1])
        config.set('Initial Settings', 'using_channel_wise_regs', str(self.using_channel_wise_regs))
        # save all config file paths
        for _fpga_index in range(self.num_fpga_tabs):
            for _asic_index in range(self.num_asic_tabs):
                if not config.has_section('Config Files'):
                    config.add_section('Config Files')
                if self.file_sync_states[_fpga_index * self.num_asic_tabs + _asic_index] == 1:
                    if self.current_config_file_path[_fpga_index * self.num_asic_tabs + _asic_index] != 'not set':
                        config.set(f'Config Files', f'fpga{_fpga_index}_asic{_asic_index}', self.current_config_file_path[_fpga_index * self.num_asic_tabs + _asic_index])

        with open(self.config_file, 'w') as configfile:
            config.write(configfile)

        # sync udp json
        with open(self.udp_config_json_name, 'r') as _udp_config:
            udp_config_json = json.load(_udp_config)
            for key in list(udp_config_json['KCU_UDP'].keys()):
                if "FPGA" in key:
                    key_index = int(key.split(" ")[1])
                    if key_index < len(self.link_ip_address_list):
                        udp_config_json['KCU_UDP'][key]['ip'] = self.link_ip_address_list[key_index]
                        udp_config_json['KCU_UDP'][key]['port'] = self.link_port_list[key_index]
            # _udp_config.write(json.dumps(udp_config_json, indent=4))
        with open(self.udp_config_json_name, 'w') as _udp_config:
            _udp_config.write(json.dumps(udp_config_json, indent=4))

app = QApplication(sys.argv)
applyModernTheme(app)  # Apply modern theme before showing the app
# add icon
app.setWindowIcon(QIcon(os.path.join('asset', 'icon.svg')))
app.setStyle('Fusion')
app.setApplicationName('H2GConfig')

dialog = H2GConfig_windows.InitialSystemDialog('config.ini', software_version)
if dialog.exec():
    system_number_info = dialog.getSystemNumber()
    main_window = MainWindow(system_number_info["fpga"], system_number_info["asic"], 'config.ini')
    main_window.show()
    sys.exit(app.exec())
