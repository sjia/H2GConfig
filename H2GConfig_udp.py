from PySide6.QtWidgets import QApplication, QMainWindow, QDialog, QVBoxLayout, QHBoxLayout, QLabel, QLineEdit, QPushButton, QTabWidget, QWidget, QSpinBox, QTabBar, QTableWidgetItem, QTableWidget, QGridLayout, QScrollArea, QHeaderView, QFileDialog, QMessageBox, QProgressBar
from PySide6.QtCore import Qt, QObject, Signal, QEventLoop
from PySide6.QtGui import QPixmap
import sys
import configparser, json
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy as np
import packetlib
import socket, time
from loguru import logger

log_format = "<level>{time:YYYY-MM-DD HH:mm:ss} | {level:.1s} | {message}</level>"

# Configure the logger with the new format
logger.remove()  # Remove default handler
logger.add(sink=sys.stderr, format=log_format)

class I2C_Dialog(QDialog):
    closed_signal = Signal()
    def closeEvent(self, event):
        self.closed_signal.emit()
        super().closeEvent(event)

def onSendConfig(self, _current_fpga, _current_asic, _show_message = True, _verbose = True):
    if "FPGA" in _current_fpga:
        _current_fpga = int(_current_fpga.split()[-1])
    if "ASIC" in _current_asic:
        _current_asic = int(_current_asic.split()[-1])

    # create progress bar dialog
    progress_dialog = I2C_Dialog(self)
    progress_bar = QProgressBar(progress_dialog)
    progress_info = QLabel(progress_dialog)
    self.cancel_i2c_send = False

    progress_dialog.setWindowTitle("Sending Configuration")
    progress_dialog.setFixedSize(500, 100)
    progress_dialog.setLayout(QVBoxLayout())
    progress_dialog.layout().addWidget(progress_bar)
    progress_dialog.layout().addWidget(progress_info)

    progress_bar.setRange(0, 100)
    progress_bar.setValue(0)

    progress_dialog.closed_signal.connect(lambda: abortSendConfig(self))

    if _show_message:
        progress_dialog.show()
    else:
        progress_dialog.hide()
    # process the showing event
    loop = QEventLoop()
    loop.processEvents()

    # send configuration
    _target_ip = self.link_ip_address_list[_current_fpga]
    _target_port = int(self.link_port_list[_current_fpga])
    _fpga_address = 0x00 + _current_fpga
    _asic_address = 0x00 + _current_asic
    _pc_ip = self.pc_ip
    _pc_port = int(self.pc_port)

    _step_progress_weight = [30, 15, 15, 15, 15, 10]
    _step_progress_info = [
        "[Step 1/6] ",
        "[Step 2/6] ",
        "[Step 3/6] ",
        "[Step 4/6] ",
        "[Step 5/6] ",
        "[Step 6/6] "
    ]

    _target_regs = self.reginfo.all_reg_dicts[_current_fpga][_current_asic]

    socket_udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    try:
        socket_udp.bind((_pc_ip, _pc_port))
        socket_udp.settimeout(3)
    
        # * Step 1: Send channel-wise or half-wise configurations
        if self.using_channel_wise_regs:
            for _key in _target_regs:
                if self.cancel_i2c_send:
                    break
                if "CM" in _key or "Channel_" in _key or "CALIB_" in _key:
                    progress_info.setText(_step_progress_info[0] + f"Setting Register {_key}")
                    loop.processEvents()
                    _reg_byte_array = _target_regs[_key]
                    # transfer byte array to int list
                    _reg_byte_array = list(_reg_byte_array)
                    _reg_addr = self.i2c_address_dict[_key]
                    if _verbose:
                        logger.info(f"Sending configuration to FPGA {_current_fpga} ASIC {_current_asic} register {_key}")
                        logger.info(f"Register address: 0x{_reg_addr:02X}")
                        logger.info(f"Data: {[hex(i) for i in _reg_byte_array]}")
                    if not packetlib.send_check_i2c_wrapper(socket_udp, _target_ip, _target_port, asic_num=_asic_address, fpga_addr = _fpga_address, sub_addr=_reg_addr, reg_addr=0x00, data=_reg_byte_array, retry=50, verbose=False):
                        logger.error("Failed to send configuration to FPGA {} ASIC {} register {}".format(_current_fpga, _current_asic, _key))
        # half-wise configuration
        else:
            for _key in _target_regs:
                if self.cancel_i2c_send:
                    break
                if "HalfWise_" in _key:
                    progress_info.setText(_step_progress_info[0] + f"Setting Register {_key}")
                    loop.processEvents()
                    _reg_byte_array = _target_regs[_key]
                    _reg_byte_array = list(_reg_byte_array)
                    _reg_addr = self.i2c_address_dict[_key]
                    if _verbose:
                        logger.info(f"Sending configuration to FPGA {_current_fpga} ASIC {_current_asic} register {_key}")
                        logger.info(f"Register address: 0x{_reg_addr:02X}")
                        logger.info(f"Data: {[hex(i) for i in _reg_byte_array]}")
                    if not packetlib.send_check_i2c_wrapper(socket_udp, _target_ip, _target_port, asic_num=_asic_address, fpga_addr = _fpga_address, sub_addr=_reg_addr, reg_addr=0x00, data=_reg_byte_array, retry=50, verbose=False):
                        logger.error("Failed to send configuration to FPGA {} ASIC {} register {}".format(_current_fpga, _current_asic, _key))

        progress_bar.setValue(_step_progress_weight[0])
        loop.processEvents()

        # * Step 2: Send global configurations
        for _key in _target_regs:
            if self.cancel_i2c_send:
                    break
            if "Global_Analog_" in _key:
                progress_info.setText(_step_progress_info[1] + f"Setting Register {_key}")
                loop.processEvents()
                _reg_byte_array = _target_regs[_key]
                _reg_byte_array = list(_reg_byte_array)
                _reg_addr = self.i2c_address_dict[_key]
                if _verbose:
                    logger.info(f"Sending configuration to FPGA {_current_fpga} ASIC {_current_asic} register {_key}")
                    logger.info(f"Register address: 0x{_reg_addr:02X}")
                    logger.info(f"Data: {[hex(i) for i in _reg_byte_array]}")
                if not packetlib.send_check_i2c_wrapper(socket_udp, _target_ip, _target_port, asic_num=_asic_address, fpga_addr = _fpga_address, sub_addr=_reg_addr, reg_addr=0x00, data=_reg_byte_array, retry=50, verbose=False):
                    logger.error("Failed to send configuration to FPGA {} ASIC {} register {}".format(_current_fpga, _current_asic, _key))
                    # print("Failed to send configuration to FPGA {} ASIC {} register {}".format(_current_fpga, _current_asic, _key))

        progress_bar.setValue(_step_progress_weight[1])
        loop.processEvents()

        # * Step 3: Send digital configurations
        for _key in _target_regs:
            if self.cancel_i2c_send:
                    break
            if "Digital_Half_" in _key:
                progress_info.setText(_step_progress_info[2] + f"Setting Register {_key}")
                loop.processEvents()
                _reg_byte_array = _target_regs[_key]
                _reg_byte_array = list(_reg_byte_array)
                _reg_addr = self.i2c_address_dict[_key]
                if _verbose:
                    logger.info(f"Sending configuration to FPGA {_current_fpga} ASIC {_current_asic} register {_key}")
                    logger.info(f"Register address: 0x{_reg_addr:02X}")
                    logger.info(f"Data: {[hex(i) for i in _reg_byte_array]}")
                if not packetlib.send_check_i2c_wrapper(socket_udp, _target_ip, _target_port, asic_num=_asic_address, fpga_addr = _fpga_address, sub_addr=_reg_addr, reg_addr=0x00, data=_reg_byte_array, retry=50, verbose=False):
                    logger.error("Failed to send configuration to FPGA {} ASIC {} register {}".format(_current_fpga, _current_asic, _key))
                    # print("Failed to send configuration to FPGA {} ASIC {} register {}".format(_current_fpga, _current_asic, _key))

        progress_bar.setValue(_step_progress_weight[2])
        loop.processEvents()

        # * Step 4: Send reference voltage
        for _key in _target_regs:
            if self.cancel_i2c_send:
                    break
            if "Reference_Voltage_" in _key:
                progress_info.setText(_step_progress_info[3] + f"Setting Register {_key}")
                loop.processEvents()
                _reg_byte_array = _target_regs[_key]
                _reg_byte_array = list(_reg_byte_array)
                _reg_addr = self.i2c_address_dict[_key]
                if _verbose:
                    logger.info(f"Sending configuration to FPGA {_current_fpga} ASIC {_current_asic} register {_key}")
                    logger.info(f"Register address: 0x{_reg_addr:02X}")
                    logger.info(f"Data: {[hex(i) for i in _reg_byte_array]}")
                if not packetlib.send_check_i2c_wrapper(socket_udp, _target_ip, _target_port, asic_num=_asic_address, fpga_addr = _fpga_address, sub_addr=_reg_addr, reg_addr=0x00, data=_reg_byte_array, retry=50, verbose=False):
                    logger.error("Failed to send configuration to FPGA {} ASIC {} register {}".format(_current_fpga, _current_asic, _key))
                    # print("Failed to send configuration to FPGA {} ASIC {} register {}".format(_current_fpga, _current_asic, _key))
        
        progress_bar.setValue(_step_progress_weight[3])
        loop.processEvents()

        # * Step 5: Send Master TDC configuration
        for _key in _target_regs:
            if self.cancel_i2c_send:
                    break
            if "Master_TDC_" in _key:
                progress_info.setText(_step_progress_info[4] + f"Setting Register {_key}")
                loop.processEvents()
                _reg_byte_array = _target_regs[_key]
                _reg_byte_array = list(_reg_byte_array)
                _reg_addr = self.i2c_address_dict[_key]
                if _verbose:
                    logger.info(f"Sending configuration to FPGA {_current_fpga} ASIC {_current_asic} register {_key}")
                    logger.info(f"Register address: 0x{_reg_addr:02X}")
                    logger.info(f"Data: {[hex(i) for i in _reg_byte_array]}")
                if not packetlib.send_check_i2c_wrapper(socket_udp, _target_ip, _target_port, asic_num=_asic_address, fpga_addr = _fpga_address, sub_addr=_reg_addr, reg_addr=0x00, data=_reg_byte_array, retry=50, verbose=False):
                    logger.error("Failed to send configuration to FPGA {} ASIC {} register {}".format(_current_fpga, _current_asic, _key))
                    # print("Failed to send configuration to FPGA {} ASIC {} register {}".format(_current_fpga, _current_asic, _key))

        progress_bar.setValue(_step_progress_weight[4])
        loop.processEvents()

        # * Step 6: Send TOP configuration
        for _key in _target_regs:
            if self.cancel_i2c_send:
                    break
            if "Top" in _key:
                progress_info.setText(_step_progress_info[5] + f"Setting Register {_key}")
                loop.processEvents()
                _reg_byte_array = _target_regs[_key]
                _reg_byte_array = list(_reg_byte_array)
                _reg_addr = self.i2c_address_dict[_key]
                if _verbose:
                    logger.info(f"Sending configuration to FPGA {_current_fpga} ASIC {_current_asic} register {_key}")
                    logger.info(f"Register address: 0x{_reg_addr:02X}")
                    logger.info(f"Data: {[hex(i) for i in _reg_byte_array]}")
                if not packetlib.send_check_i2c_top_wrapper(socket_udp, _target_ip, _target_port, asic_num=_asic_address, fpga_addr = _fpga_address, sub_addr=_reg_addr, reg_addr=0x00, data=_reg_byte_array, retry=50, verbose=False):
                    logger.error("Failed to send configuration to FPGA {} ASIC {} register {}".format(_current_fpga, _current_asic, _key))
                    # print("Failed to send configuration to FPGA {} ASIC {} register {}".format(_current_fpga, _current_asic, _key))
        
        progress_bar.setValue(_step_progress_weight[5])
        loop.processEvents()

    except Exception as e:
        QMessageBox.critical(self, "Error", "Failed to send configuration to FPGA {} ASIC {}\n{}".format(_current_fpga, _current_asic, e))

    finally:
        socket_udp.close()
        progress_dialog.close()

def abortSendConfig(self):
    self.cancel_i2c_send = True

def onReadBack(self, _current_fpga, _current_asic, _show_message = True, _verbose = True):
    if "FPGA" in _current_fpga:
        _current_fpga = int(_current_fpga.split()[-1])
    if "ASIC" in _current_asic:
        _current_asic = int(_current_asic.split()[-1])

    _target_regs = self.reginfo.all_reg_dicts[_current_fpga][_current_asic]

    _target_ip = self.link_ip_address_list[_current_fpga]
    _target_port = int(self.link_port_list[_current_fpga])
    _fpga_address = 0x00 + _current_fpga
    _asic_address = 0x00 + _current_asic
    _pc_ip = self.pc_ip
    _pc_port = int(self.pc_port)

    socket_udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    reg_save_path = QFileDialog.getSaveFileName(self, "Save Register File", "reg.txt", "Text Files (*.txt)")[0]
    if reg_save_path == "":
        return

    try:
        socket_udp.bind((_pc_ip, _pc_port))
        socket_udp.settimeout(3)
        # * Step 1: Read channel-wise or half-wise configurations
        packetlib.read_save_all_i2c(reg_save_path, socket_udp, _target_ip, _target_port, _current_asic, _fpga_address)
    finally:
        socket_udp.close()
        


def onSendAllConfig(self, _show_message = True):
    progress_dialog = QDialog(self)
    progress_bar = QProgressBar(progress_dialog)
    progress_info = QLabel(progress_dialog)

    progress_dialog.setWindowTitle("Sending Configuration")
    progress_dialog.setFixedSize(500, 100)
    progress_dialog.setLayout(QVBoxLayout())
    progress_dialog.layout().addWidget(progress_bar)
    progress_dialog.layout().addWidget(progress_info)

    progress_bar.setRange(0, 100)
    progress_bar.setValue(0)

    if _show_message:
        progress_dialog.show()
    else:
        progress_dialog.hide()

    for _fpga in range(self.num_fpga_tabs):
        for _asic in range(self.num_asic_tabs):
            progress_bar.setValue(100*(_fpga*self.num_asic_tabs + _asic)/(self.num_fpga_tabs*self.num_asic_tabs))
            progress_info.setText("Sending configuration to FPGA {} ASIC {}".format(_fpga, _asic))
            loop = QEventLoop()
            loop.processEvents()
            _fpga_str = "FPGA {}".format(_fpga)
            _asic_str = "ASIC {}".format(_asic)
            onSendConfig(self, _fpga_str, _asic_str, _show_message=False)

    progress_bar.setValue(100)
    progress_info.setText("Configuration sent to all FPGA and ASIC")
    loop.processEvents()

    progress_dialog.close()

def ReadPedestal(self, _current_fpga, _current_asic, _reg_runLR, _reg_offLR, _event_num, _fragment_life, _logger):
    logger.warning("Reading pedestal")
    logger.warning(f"Current FPGA: {_current_fpga}")
    logger.warning(f"Current ASIC: {_current_asic}")
    logger.warning(f"Event number: {_event_num}")
    _target_ip = self.link_ip_address_list[_current_fpga]
    _target_port = int(self.link_port_list[_current_fpga])
    _fpga_address = 0x00 + _current_fpga
    _asic_address = 0x00 + _current_asic
    _pc_ip = self.pc_ip
    _pc_port = int(self.pc_port)

    logger.debug(f"Target IP: {_target_ip}")
    logger.debug(f"Target port: {_target_port}")
    logger.debug(f"FPGA address: {_fpga_address}")
    logger.debug(f"ASIC address: {_asic_address}")
    logger.debug(f"PC IP: {_pc_ip}")
    logger.debug(f"PC port: {_pc_port}")

    _socket_udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    _socket_udp.bind((_pc_ip, _pc_port))
    _socket_udp.settimeout(3)

    _asic_index = _current_asic
    _ip = _target_ip
    _port = _target_port

    if not packetlib.send_check_DAQ_gen_params(_socket_udp, _ip, _port, 0x00, fpga_addr=_current_fpga, data_coll_en=0xFF, trig_coll_en=0x00, daq_fcmd=75, gen_preimp_en=0, gen_pre_interval = 10, gen_nr_of_cycle=_event_num, gen_pre_fcmd=75, gen_fcmd=75,gen_interval=200, daq_push_fcmd=75, machine_gun=0x00, verbose=False):
        logger.warning(f"Failed to set up the generator")

    if not packetlib.send_check_i2c_wrapper(_socket_udp, _ip, _port, asic_num=_asic_index, fpga_addr = _fpga_address, sub_addr=packetlib.subblock_address_dict["Top"], reg_addr=0x00, data=_reg_runLR, retry=50, verbose=False):
        _logger.warning(f"Failed to turn on LR")
    packetlib.clean_socket(_socket_udp)

    if not packetlib.send_daq_gen_start_stop(_socket_udp, _ip, _port, asic_num=0, fpga_addr = _fpga_address, daq_push=0x00, gen_start_stop=1, daq_start_stop=0xFF, verbose=False):
        _logger.warning("Failed to start the generator")

    extracted_payloads_pool = []
    event_fragment_pool     = []
    fragment_life_dict      = {}

    current_half_packet_num = 0
    current_event_num = 0

    all_chn_value_0_array = np.zeros((_event_num, 76*self.num_asic_tabs))
    all_chn_value_1_array = np.zeros((_event_num, 76*self.num_asic_tabs))
    all_chn_value_2_array = np.zeros((_event_num, 76*self.num_asic_tabs))
    hamming_code_array    = np.zeros((_event_num, 12))

    while True:
        try:
            data_packet, rec_addr    = _socket_udp.recvfrom(8192)
            extracted_payloads_pool += packetlib.extract_raw_payloads(data_packet)
            while len(extracted_payloads_pool) >= 5:
                candidate_packet_lines = extracted_payloads_pool[:5]
                is_packet_good, event_fragment = packetlib.check_event_fragment(candidate_packet_lines)
                if is_packet_good:
                    event_fragment_pool.append(event_fragment)
                    current_half_packet_num += 1
                    extracted_payloads_pool = extracted_payloads_pool[5:]
                else:
                    _logger.warning("Warning: Event fragment is not good")
                    extracted_payloads_pool = extracted_payloads_pool[1:]
            indices_to_delete = set()
            if len(event_fragment_pool) >= 4:
                event_fragment_pool = sorted(event_fragment_pool, key=lambda x: x[0][3:7])
            i = 0
            while i <= len(event_fragment_pool) - 4:
                timestamp0 = event_fragment_pool[i][0][4] << 24 | event_fragment_pool[i][0][5] << 16 | event_fragment_pool[i][0][6] << 8 | event_fragment_pool[i][0][7]
                timestamp1 = event_fragment_pool[i+1][0][4] << 24 | event_fragment_pool[i+1][0][5] << 16 | event_fragment_pool[i+1][0][6] << 8 | event_fragment_pool[i+1][0][7]
                timestamp2 = event_fragment_pool[i+2][0][4] << 24 | event_fragment_pool[i+2][0][5] << 16 | event_fragment_pool[i+2][0][6] << 8 | event_fragment_pool[i+2][0][7]
                timestamp3 = event_fragment_pool[i+3][0][4] << 24 | event_fragment_pool[i+3][0][5] << 16 | event_fragment_pool[i+3][0][6] << 8 | event_fragment_pool[i+3][0][7]
                if timestamp0 == timestamp1 and timestamp0 == timestamp2 and timestamp0 == timestamp3:
                    id_str = f"{timestamp0:08X}"
                    for _half in range(4):
                        extracted_data = packetlib.assemble_data_from_40bytes(event_fragment_pool[i+_half], verbose=False)
                        extracted_values = packetlib.extract_values(extracted_data["_extraced_160_bytes"], verbose=False)
                        uni_chn_base = (extracted_data["_header"] - 0xA0) * 76 + (extracted_data["_packet_type"] - 0x24) * 38
                        for j in range(len(extracted_values["_extracted_values"])):
                            all_chn_value_0_array[current_event_num][j+uni_chn_base] = extracted_values["_extracted_values"][j][1]
                            all_chn_value_1_array[current_event_num][j+uni_chn_base] = extracted_values["_extracted_values"][j][2]
                            all_chn_value_2_array[current_event_num][j+uni_chn_base] = extracted_values["_extracted_values"][j][3]
                        hamming_code_array[current_event_num][_half*3+0] =  packetlib.DaqH_get_H1(extracted_values["_DaqH"])
                        hamming_code_array[current_event_num][_half*3+1] =  packetlib.DaqH_get_H2(extracted_values["_DaqH"])
                        hamming_code_array[current_event_num][_half*3+2] =  packetlib.DaqH_get_H3(extracted_values["_DaqH"])
                    indices_to_delete.update([i, i+1, i+2, i+3])
                    current_event_num += 1
                    i += 4
                else:
                    if timestamp0 in fragment_life_dict:
                        if fragment_life_dict[timestamp0] >= _fragment_life - 1:
                            indices_to_delete.update([i])
                            del fragment_life_dict[timestamp0]
                        else:
                            fragment_life_dict[timestamp0] += 1
                    else:
                        fragment_life_dict[timestamp0] = 1
                    i += 1
            for index in sorted(indices_to_delete, reverse=True):
                del event_fragment_pool[index]
            if current_event_num == _event_num:
                break;      
        except Exception as e:
            _logger.warning("Exception in receiving data")
            _logger.warning(e)
            _logger.warning('Packet received: ' + str(current_half_packet_num))
            _logger.warning('left fragments:' + str(len(event_fragment_pool)))
            _logger.warning("current event num:" + str(current_event_num))
            measurement_good_flag = False
            break

    if not np.all(hamming_code_array == 0):
        _logger.warning("Hamming code error detected!")
        measurement_good_flag = False
    if not packetlib.send_daq_gen_start_stop(_socket_udp, _ip, _port, asic_num=0, fpga_addr = _fpga_address, daq_push=0x00, gen_start_stop=0, daq_start_stop=0x00, verbose=False):
        _logger.warning("Failed to stop the generator")

    if not packetlib.send_check_i2c_wrapper(_socket_udp, _ip, _port, asic_num=_asic_index, fpga_addr = _fpga_address, sub_addr=packetlib.subblock_address_dict["Top"], reg_addr=0x00, data=_reg_offLR, retry=50, verbose=False):
        _logger.warning(f"Failed to turn off LR")

    _val0_mean_list = []
    _val0_err_list  = []
    for _chn in range(76*self.num_asic_tabs):
        _candidate_values = []
        for _event in range(_event_num):
            if np.all(hamming_code_array[_event] == 0):
                _candidate_values.append(all_chn_value_0_array[_event][_chn])
        if len(_candidate_values) > 0:
            _val0_mean_list.append(np.mean(_candidate_values))
            _val0_err_list.append(np.std(_candidate_values))
        else:
            _logger.warning(f"Channel {_chn} has no valid data")
            _val0_mean_list.append(0)
            _val0_err_list.append(0)
    return _val0_mean_list, _val0_err_list

