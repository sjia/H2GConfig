from PySide6.QtWidgets import QDialog, QVBoxLayout, QHBoxLayout, QLabel, QPushButton, QSpinBox, QSizePolicy, QSizePolicy, QSizePolicy, QSizePolicy
from PySide6.QtGui import QCursor, QPalette, QPixmap, QResizeEvent
from PySide6.QtCore import Qt
import configparser

class InitialSystemDialog(QDialog):
    def __init__(self, config_file, verison):
        super().__init__()
        self.config_file = config_file
        self.version = verison
        self.setGeometry(100, 100, 600, 400)
        self.initUI()
        self.loadConfig()

    def initUI(self):
        self.original_width = 600
        self.original_height = 400

        self.background_label = QLabel(self)
        self.background_label.setPixmap(QPixmap("asset/loading_page.png"))
        self.background_label.setScaledContents(True)
        self.background_label.setGeometry(0, 0, 600, 400)

        # FPGA number spinbox and label
        self.fpga_num_label = QLabel("Number of KCUs:", self)
        self.fpga_num_label.setStyleSheet("font-size: 14px; font-family: 'DejaVu Sans Mono';")
        self.fpga_num_label.setGeometry(380, 50, 200, 30)

        self.fpga_num_spinbox = QSpinBox(self)
        self.fpga_num_spinbox.setStyleSheet("font-size: 14px; font-family: 'DejaVu Sans Mono';")
        self.fpga_num_spinbox.setRange(1, 8)
        self.fpga_num_spinbox.setValue(1)
        self.fpga_num_spinbox.setFixedWidth(50)
        self.fpga_num_spinbox.setGeometry(380, 100, 200, 30)

        # ASIC number spinbox and label
        self.asic_num_label = QLabel("Number of ASICs per KCU:", self)
        self.asic_num_label.setStyleSheet("font-size: 14px; font-family: 'DejaVu Sans Mono';")
        self.asic_num_label.setGeometry(380, 200, 200, 30)

        self.asic_num_spinbox = QSpinBox(self)
        self.asic_num_spinbox.setStyleSheet("font-size: 14px; font-family: 'DejaVu Sans Mono';")
        self.asic_num_spinbox.setRange(1, 8)
        self.asic_num_spinbox.setValue(1)
        self.asic_num_spinbox.setFixedWidth(50)
        self.asic_num_spinbox.setGeometry(380, 250, 200, 30)

        # Confirm button
        self.confirm_button = QPushButton("Confirm", self)
        self.confirm_button.setStyleSheet("font-size: 14px; font-family: 'DejaVu Sans Mono';")
        self.confirm_button.setGeometry(380, 350, 200, 30)
        self.confirm_button.clicked.connect(self.onAccepted)

        # version info
        self.version_label = QLabel("v " + self.version, self)
        self.version_label.setStyleSheet("font-size: 14px; font-family: 'DejaVu Sans Mono';")
        self.version_label.setGeometry(30, 350, 200, 30)

        self.setWindowTitle("Initial System Dialog")

    def getSystemNumber(self):
        return {"fpga": self.fpga_num_spinbox.value(), "asic": self.asic_num_spinbox.value()}
    
    def resizeEvent(self, event: QResizeEvent):
        new_width = event.size().width()
        new_height = event.size().height()

        width_ratio = new_width / self.original_width
        height_ratio = new_height / self.original_height

        self.background_label.setGeometry(0, 0, new_width, new_height)

        self.fpga_num_label.setGeometry(int(380 * width_ratio), int(50 * height_ratio), int(200 * width_ratio), int(30 * height_ratio))
        self.fpga_num_spinbox.setGeometry(int(380 * width_ratio), int(100 * height_ratio), int(200 * width_ratio), int(30 * height_ratio))

        self.asic_num_label.setGeometry(int(380 * width_ratio), int(200 * height_ratio), int(200 * width_ratio), int(30 * height_ratio))
        self.asic_num_spinbox.setGeometry(int(380 * width_ratio), int(250 * height_ratio), int(200 * width_ratio), int(30 * height_ratio))

        self.confirm_button.setGeometry(int(380 * width_ratio), int(350 * height_ratio), int(200 * width_ratio), int(30 * height_ratio))

        self.version_label.setGeometry(int(30 * width_ratio), int(350 * height_ratio), int(200 * width_ratio), int(30 * height_ratio))

    
    def loadConfig(self):
        config = configparser.ConfigParser()
        config.read(self.config_file)
        num_fpgas = config.getint("Initial Settings", "num_fpgas", fallback=1)
        num_asics = config.getint("Initial Settings", "num_asics", fallback=1)
        self.fpga_num_spinbox.setValue(num_fpgas)
        self.asic_num_spinbox.setValue(num_asics)

    def onAccepted(self):
        config = configparser.ConfigParser()
        config.read(self.config_file)
        if not config.has_section("Initial Settings"):
            config.add_section("Initial Settings")
        config.set("Initial Settings", "num_fpgas", str(self.fpga_num_spinbox.value()))
        config.set("Initial Settings", "num_asics", str(self.asic_num_spinbox.value()))
        with open(self.config_file, "w") as configfile:
            config.write(configfile)
        self.accept()