import socket, time

def udp_client(server_address, server_port):
    # Create a UDP socket
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    
    # Bind the socket to a specific address and port
    client_ip = '127.0.0.1'
    client_port = 12000  # This is the port on which the client will listen
    client_socket.bind((client_ip, client_port))
    
    # Define the message to send to the server
    message = b"Hello, server!"

    try:
        # Send data
        # print(f"Sending to {server_address}:{server_port} - {message}")
        # sent = client_socket.sendto(message, (server_address, server_port))

        # Receive response
        print("Waiting for response")
        last_data_packet = bytearray()
        while True:
            data, server = client_socket.recvfrom(1024)  # Buffer size is 40 bytes, as expected for the response
            if data[2] == 0x11:
                print("Received last data packet")
                last_data_packet = bytearray()
                for i in range(40):
                    if i == 2:
                        last_data_packet.append(0x10)
                    else:
                        last_data_packet.append(data[i])


            if data[2] == 0x10:
                # send back the last data packet
                print("Received data request")
                time.sleep(0.5)
                client_socket.sendto(last_data_packet, server)

            print(f"Received {data} from {server}")

    finally:
        print("Closing socket")
        client_socket.close()

# Configuration
server_address = '127.0.0.1'  # Server address to send the data
server_port = 12000          # Port number where the server is listening

# Run the client
udp_client(server_address, server_port)
