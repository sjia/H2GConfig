from PySide6.QtWidgets import QApplication, QMainWindow, QDialog, QVBoxLayout, QHBoxLayout, QLabel, QLineEdit, QPushButton, QTabWidget, QWidget, QSpinBox, QTabBar, QTableWidgetItem, QTableWidget, QGridLayout, QScrollArea, QHeaderView, QFileDialog, QMessageBox, QAbstractItemView
from PySide6.QtCore import Qt, QObject, Signal, QEvent, QCoreApplication
from PySide6.QtGui import QPixmap, QWheelEvent, QMouseEvent
import sys
import configparser, json
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy as np
from loguru import logger
import H2GConfig_udp
import packetlib

log_format = "<level>{time:YYYY-MM-DD HH:mm:ss} | {level:.1s} | {message}</level>"

# Configure the logger with the new format
logger.remove()  # Remove default handler
logger.add(sink=sys.stderr, format=log_format)

def UniChannelNum2RegKey(mainwindow, channel_num):
    # create reverse dict
    _mapping_chn2key = {}
    _current_counter = 0
    for _key in mainwindow.i2c_address_dict:
        if "CM_" in _key or "Channel_" in _key or "CALIB_" in _key:
            _reverse_key = str(_current_counter)
            _mapping_chn2key[_reverse_key] = _key
            _current_counter += 1
    return _mapping_chn2key[str(channel_num)]

class RegInfo(QObject):
    update_reg_signal = Signal(int, int, int)
    update_sync_signal = Signal(int, int, int)
    def __init__(self):
        super().__init__()
        self.all_reg_dicts = []
        self.all_reg_names = []

    def confirmSync(self, fpga_index, asic_index, is_synced=True):
        state_flag = 1 if is_synced else 2
        self.update_sync_signal.emit(fpga_index, asic_index, state_flag)

    def confirmUnsync(self, fpga_index, asic_index, is_unsynced=True):
        state_flag = 2 if is_unsynced else 1
        self.update_sync_signal.emit(fpga_index, asic_index, state_flag)

    def updateReg(self, fpga_index, asic_index, new_reg_dict):
        logger.info(f"Update reg: FPGA {fpga_index}, ASIC {asic_index}")
        self.all_reg_dicts[fpga_index][asic_index] = new_reg_dict.copy()
        self.update_reg_signal.emit(fpga_index, asic_index, 0)
        # self.update_sync_signal.emit(fpga_index, asic_index, True)

    def flipBit(self, fpga_index, asic_index, reg_key, byte_index, bit_index):
        logger.info(f"Flip bit: FPGA {fpga_index}, ASIC {asic_index}, Reg: {reg_key}, Byte: {byte_index}, Bit: {bit_index}")
        logger.debug(f"Before: {self.all_reg_dicts[fpga_index][0][reg_key][byte_index]:02x}")
        logger.debug(f"Before: {self.all_reg_dicts[fpga_index][1][reg_key][byte_index]:02x}")
        self.all_reg_dicts[fpga_index][asic_index][reg_key][byte_index] ^= 1 << bit_index
        logger.debug(f"After: {self.all_reg_dicts[fpga_index][0][reg_key][byte_index]:02x}")
        logger.debug(f"After: {self.all_reg_dicts[fpga_index][1][reg_key][byte_index]:02x}")
        self.update_reg_signal.emit(fpga_index, asic_index, byte_index)
        self.update_sync_signal.emit(fpga_index, asic_index, 2)

    def exportJSON(self, fpga_index, asic_index):
        _output_dict = {}
        for key in self.all_reg_dicts[fpga_index][asic_index]:
            _bytes_str = ''
            for byte in self.all_reg_dicts[fpga_index][asic_index][key]:
                # 2-bit hex space separated
                _bytes_str += f"{byte:02x} "
            _bytes_str = _bytes_str[:-1]
            _output_dict[regNameTranslation2JSON(key)] = _bytes_str
        return _output_dict

class BitPushButton(QPushButton):
    flip_bit_signal = Signal(int, int, str, int, int) # fpga_index, asic_index, reg_index, byte_index, bit_index
    def __init__(self, parent, bit_value, fpga_index, asic_index, reg_key, byte_index, bit_index):
        super().__init__(parent)
        if bit_value != 0 and bit_value != 1:
            print(f"Invalid bit value: {bit_value}")
        self.bit_value = bit_value
        self.setStyleSheet("background-color: #f0f0f0; font-size: 14px; font-family: 'Segoe UI';color: black;")
        self.setText(str(bit_value))
        self.fpga_index = fpga_index
        self.asic_index = asic_index
        self.reg_key = reg_key
        self.byte_index = byte_index
        self.bit_index = bit_index
        self.clicked.connect(self.on_clicked)

    def on_clicked(self):
        if self.bit_value == 0:
            self.bit_value = 1
        else:
            self.bit_value = 0
        self.setText(str(self.bit_value))
        self.flip_bit_signal.emit(self.fpga_index, self.asic_index, self.reg_key, self.byte_index, self.bit_index)

class LedIndicator(QLabel):
    def __init__(self, parent=None):
        super(LedIndicator, self).__init__(parent)
        self.change_state(0)

    def change_state(self, state):
        if state == 0:
            self.setStyleSheet("background-color: grey; border-radius: 10px; border: 1px solid black;")
            self.setText("File Not Set")
            self.setAlignment(Qt.AlignCenter)
        elif state == 1:
            self.setStyleSheet("background-color: green; border-radius: 10px; border: 1px solid black;")
            self.setText("Synced to File")
            self.setAlignment(Qt.AlignCenter)
        elif state == 2:
            self.setStyleSheet("background-color: red; border-radius: 10px; border: 1px solid black;")
            self.setText("Out of Sync")
            self.setAlignment(Qt.AlignCenter)
        

def printAllRegContent(self):
    sublist_num = len(self.reginfo.all_reg_dicts)
    print(f"Total number of sublists: {sublist_num}")
    sublist_asic_num = len(self.reginfo.all_reg_dicts[0])
    print(f"Total number of ASICs in each sublist: {sublist_asic_num}")
    i = 0
    j = 0
    print(f"Sublist {i}, ASIC {j}")
    for key in self.reginfo.all_reg_dicts[i][j]:
        print(f"Register: {key}")
        print(self.reginfo.all_reg_dicts[i][j][key])

def regNameTranslation2Dict(reg_name):
    output_name = reg_name.replace(' ', '')
    return output_name

def regNameTranslation2JSON(reg_name):
    output_name = reg_name
    while len(output_name) < 20:
        output_name += ' '
    return output_name

def loadConfigFile(self, _current_fpga, _current_asic, _config_file):

    with open(_config_file, 'r') as f:
        _config = json.load(f)
        _ip_address = _config['UDP Settings']['IP Address']
        _port = _config['UDP Settings']['Port']
        _fpga_index = _config['Target ASIC']['FPGA Address']
        _asic_index = _config['Target ASIC']['ASIC Address']
        is_fully_synced = True
        if _fpga_index != _current_fpga or _asic_index != _current_asic:
            # let user to choice whether to load the config file
            _reply = QMessageBox.question(self, "Warning", "Config file is not for the current FPGA and ASIC. Do you want to load it anyway?", QMessageBox.Yes | QMessageBox.No)
            if _reply == QMessageBox.No:
                return
        if _ip_address != self.link_ip_address_list[_current_fpga] or _port != self.link_port_list[_current_fpga]:
            # let user to choice whether to load the config file
            _reply = QMessageBox.question(self, "Warning", "Config file is not for the current IP address and port. Do you want to use the IP address and port in the config file?", QMessageBox.Yes | QMessageBox.No)
            if _reply == QMessageBox.No:
                is_fully_synced = False
            else:
                self.link_ip_address_list[_current_fpga] = _ip_address
                self.link_port_list[_current_fpga] = _port
                self.link_sidebar_link_ip_label.setText(f"{_ip_address}")
                self.link_sidebar_link_port_label.setText(f"{_port}")
                # set other asic with same FPGA to out of sync
                for i in range(self.num_asic_tabs):
                    if i == _current_asic:
                        continue
                    self.reginfo.confirmUnsync(_current_fpga, i)
        self.current_config_file_path[_current_fpga * self.num_asic_tabs + _current_asic] = _config_file
        self.link_sidebar_config_file_label.setText(self.current_config_file_path[_current_fpga * self.num_asic_tabs + _current_asic])
        new_reg_dict = loadConfigJson(self, _config)
        self.reginfo.updateReg(_current_fpga, _current_asic, new_reg_dict)
        self.reginfo.confirmSync(_current_fpga, _current_asic, is_fully_synced)

    
def onLoadConfigButtonClicked(self, _current_fpga, _current_asic, _reg_info):
    if "FPGA" in _current_fpga:
        _current_fpga = int(_current_fpga.split()[-1])
    if "ASIC" in _current_asic:
        _current_asic = int(_current_asic.split()[-1])
    # print(f"FPAG: {_current_fpga}, ASIC: {_current_asic}")
    _config_file = QFileDialog.getOpenFileName(self, 'Open Config File', './', 'Config Files (*.json)')[0]
    if _config_file:
        is_fully_synced = True
        with open(_config_file, 'r') as f:
            _config = json.load(f)
            _ip_address = _config['UDP Settings']['IP Address']
            _port = _config['UDP Settings']['Port']
            _fpga_index = _config['Target ASIC']['FPGA Address']
            _asic_index = _config['Target ASIC']['ASIC Address']
            if _fpga_index != _current_fpga or _asic_index != _current_asic:
                # let user to choice whether to load the config file
                _reply = QMessageBox.question(self, "Warning", "Config file is not for the current FPGA and ASIC. Do you want to load it anyway?", QMessageBox.Yes | QMessageBox.No)
                if _reply == QMessageBox.No:
                    return
                else:
                    is_fully_synced = False
            if _ip_address != self.link_ip_address_list[_current_fpga] or _port != self.link_port_list[_current_fpga]:
                # let user to choice whether to load the config file
                _reply = QMessageBox.question(self, "Warning", "Config file is not for the current IP address and port. Do you want to use the IP address and port in the config file?", QMessageBox.Yes | QMessageBox.No)
                if _reply == QMessageBox.No:
                    is_fully_synced = False
                else:
                    self.link_ip_address_list[_current_fpga] = _ip_address
                    self.link_port_list[_current_fpga] = _port
                    self.link_sidebar_link_ip_label.setText(f"{_ip_address}")
                    self.link_sidebar_link_port_label.setText(f"{_port}")
                    # set other asic with same FPGA to out of sync
                    for i in range(self.num_asic_tabs):
                        if i == _current_asic:
                            continue
                        self.reginfo.confirmUnsync(_current_fpga, i)
            self.current_config_file_path[_current_fpga * self.num_asic_tabs + _current_asic] = _config_file
            self.link_sidebar_config_file_label.setText(self.current_config_file_path[_current_fpga * self.num_asic_tabs + _current_asic])
            new_reg_dict = loadConfigJson(self, _config)
            _reg_info.updateReg(_current_fpga, _current_asic, new_reg_dict)
            _reg_info.confirmSync(_current_fpga, _current_asic, is_fully_synced)
        onHumanModeButtonClicked(self, _current_fpga, _current_asic)

def onSaveAsConfigButtonClicked(self, _current_fpga, _current_asic):
    if "FPGA" in _current_fpga:
        _current_fpga = int(_current_fpga.split()[-1])
    if "ASIC" in _current_asic:
        _current_asic = int(_current_asic.split()[-1])
    _config_file = QFileDialog.getSaveFileName(self, 'Save Config File', './', 'Config Files (*.json)')[0]
    if _config_file:
        if not _config_file.endswith('.json'):
            _config_file += '.json'
        _output_json = {}
        _output_UDP = {}
        _output_UDP['IP Address'] = self.link_ip_address_list[_current_fpga]
        _output_UDP['Port'] = self.link_port_list[_current_fpga]
        _output_json['UDP Settings'] = _output_UDP
        _output_index = {}
        _output_index['FPGA Address'] = _current_fpga
        _output_index['ASIC Address'] = _current_asic
        _output_json['Target ASIC'] = _output_index
        _output_json['Register Settings'] = self.reginfo.exportJSON(_current_fpga, _current_asic)
        with open(_config_file, 'w') as f:
            json.dump(_output_json, f, indent=4)
        self.current_config_file_path[_current_fpga * self.num_asic_tabs + _current_asic] = _config_file
        self.link_sidebar_config_file_label.setText(self.current_config_file_path[_current_fpga * self.num_asic_tabs + _current_asic])
        self.reginfo.confirmSync(_current_fpga, _current_asic)
        QMessageBox.information(self, "Information", f"Config file saved to {_config_file}")
    else:
        QMessageBox.warning(self, "Warning", "No file selected.")

def onSaveConfigButtonClicked(self, _current_fpga, _current_asic, _reg_info):
    if "FPGA" in _current_fpga:
        _current_fpga = int(_current_fpga.split()[-1])
    if "ASIC" in _current_asic:
        _current_asic = int(_current_asic.split()[-1])
    _config_file = self.current_config_file_path[_current_fpga * self.num_asic_tabs + _current_asic]
    if (_config_file == 'not set'):
        QMessageBox.warning(self, "Warning", "No config file loaded.")
    else:
        _output_json = {}
        _output_UDP = {}
        _output_UDP['IP Address'] = self.link_ip_address_list[_current_fpga]
        _output_UDP['Port'] = self.link_port_list[_current_fpga]
        _output_json['UDP Settings'] = _output_UDP
        _output_index = {}
        _output_index['FPGA Address'] = _current_fpga
        _output_index['ASIC Address'] = _current_asic
        _output_json['Target ASIC'] = _output_index
        _output_json['Register Settings'] = _reg_info.exportJSON(_current_fpga, _current_asic)

        # if the file is used also by other FPGA and ASIC, update their sync status
        for i in range(self.num_fpga_tabs):
            for j in range(self.num_asic_tabs):
                if i == _current_fpga and j == _current_asic:
                    continue
                if self.current_config_file_path[i * self.num_asic_tabs + j] == _config_file:
                    _reg_info.confirmSync(i, j, False)
        with open(_config_file, 'w') as f:
            json.dump(_output_json, f, indent=4)
        _reg_info.confirmSync(_current_fpga, _current_asic)
        QMessageBox.information(self, "Information", f"Config file saved to {_config_file}")

def loadConfigJson(self, _config):
    _output_dict = {}
    for _key in _config["Register Settings"]:
        byte_str = _config["Register Settings"][_key]
        # split by space
        byte_list = byte_str.split()
        byte_array = bytearray()
        for byte in byte_list:
            byte_array.append(int(byte, 16))
        _output_dict[regNameTranslation2Dict(_key)] = byte_array
    return _output_dict

def onModeChangeButtonClicked(self, _index, _current_fpga, _current_asic):
    if _index == 1:
        onHumanModeButtonClicked(self, _current_fpga, _current_asic)
    elif _index == 0:
        _parent = self.asic_tab_layouts[_current_fpga][_current_asic]
        onRobotButtonClicked(self, _parent, _current_fpga, _current_asic)

def onRobotButtonClicked(self, parent, _current_fpga, _current_asic):
    _label = QLabel("Select a register to view its content")
    _label.setStyleSheet(self.divider_label_format)
    _label.setFixedHeight(20)
    for i in reversed(range(parent.count())):
        parent.itemAt(i).widget().deleteLater()
    parent.addWidget(_label, alignment=Qt.AlignCenter)

def onRegRefresh(self, parent, _current_fpga, _current_asic, _current_reg, _view_pos):
    _label = QLabel(f"Register: {_current_reg}")
    self.current_reg_key = _current_reg
    _label.setStyleSheet(self.module_title_format)
    _label.setFixedHeight(20)
    for i in reversed(range(parent.count())):
        parent.itemAt(i).widget().deleteLater()
    parent.addWidget(_label, alignment=Qt.AlignCenter)

    _content_table = QTableWidget(parent.widget())
    _content_table.setStyleSheet("background-color: #ffffff; font-family: 'Segoe UI'; font-size: 12px; color: #333333;")

    _content_table.setRowCount(len(self.reginfo.all_reg_dicts[_current_fpga][_current_asic][_current_reg]) * 2)
    _content_table.setColumnCount(8)
    _content_table.setHorizontalHeaderLabels(["Bit 7", "Bit 6", "Bit 5", "Bit 4", "Bit 3", "Bit 2", "Bit 1", "Bit 0"])
    _table_row_labels = []

    for i in range(len(self.reginfo.all_reg_dicts[_current_fpga][_current_asic][_current_reg])):
        _table_row_labels.append('Byte')
        _table_row_labels.append(f'{i}')
    _content_table.setVerticalHeaderLabels(_table_row_labels)
    _content_table.horizontalHeader().setDefaultAlignment(Qt.AlignCenter)
    _content_table.verticalHeader().setDefaultAlignment(Qt.AlignCenter)
    _content_table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)

    # set minimum width for each column
    for i in range(8):
        _content_table.setColumnWidth(i, 100)
    for i in range(len(self.reginfo.all_reg_dicts[_current_fpga][_current_asic][_current_reg]) * 2):
        _content_table.setRowHeight(i, 30)
    
    for i in range(len(self.reginfo.all_reg_dicts[_current_fpga][_current_asic][_current_reg]) * 2):
        for j in range(8):
            if i % 2 == 1:
                _value = self.reginfo.all_reg_dicts[_current_fpga][_current_asic][_current_reg][i // 2] >> (7 - j) & 0x1

                _item_bit_button = BitPushButton(parent.widget(), _value, _current_fpga, _current_asic, _current_reg, i // 2, 7 - j)
                _item_bit_button.flip_bit_signal.connect(
                    lambda fpga_index_val=_current_fpga, asic_index_val=_current_asic, reg_key_val=_current_reg, byte_index_val=i // 2, bit_index_val=7 - j:
                    self.reginfo.flipBit(fpga_index_val, asic_index_val, reg_key_val, byte_index_val, bit_index_val)
                )
                _content_table.setCellWidget(i, j, _item_bit_button)
            else:
                _value = self.reginfo.all_reg_names[_current_fpga][_current_asic][_current_reg][i // 2][j]
                # manual word wrap
                if len(_value) > 21:
                    _value = _value[:10] + '\n' + _value[10:20] + '\n' + _value[20:]
                    _content_table.setRowHeight(i, 70)
                elif len(_value) > 11:
                    _value = _value[:10] + '\n' + _value[10:]
                    _content_table.setRowHeight(i, 50)

                _item = QTableWidgetItem(_value)
                _item.setFlags(~Qt.ItemIsEditable)
                _item.setBackground(Qt.gray)
                _item.setFont("font-size: 10px; font-family: 'DejaVu Sans Mono';")
                _content_table.setItem(i, j, _item)

    parent.addWidget(_content_table, stretch=3)

    # Ensure all pending events are processed
    QApplication.processEvents()
    _view_pos -= 4
    # Scroll to the specified view position (_view_pos)
    if _view_pos is not None and 0 <= _view_pos < _content_table.rowCount():
        _content_table.scrollTo(_content_table.model().index(_view_pos, 0), QAbstractItemView.PositionAtCenter)

    # # Scroll to the specified view position (_view_pos)
    # if _view_pos is not None and 0 <= _view_pos < _content_table.rowCount():
    #     logger.debug(f"Scroll to item: {_view_pos}")
    #     _content_table.scrollToItem(_content_table.item(_view_pos, 0), QAbstractItemView.PositionAtCenter)


def onRegButtonClicked(self, parent, _current_fpga, _current_asic, _current_reg):
    # _current_reg = regNameTranslation2JSON(_current_reg)
    # print all keys in the dictionary
    # for key in self.reginfo.all_reg_dicts[_current_fpga][_current_asic]:
    #     print(f"Register: {key},")
    _label = QLabel(f"Register: {_current_reg}")
    self.current_reg_key = _current_reg
    _label.setStyleSheet(self.module_title_format)
    _label.setFixedHeight(20)
    for i in reversed(range(parent.count())):
        parent.itemAt(i).widget().deleteLater()
    parent.addWidget(_label, alignment=Qt.AlignCenter)

    _content_table = QTableWidget(parent.widget())
    _content_table.setStyleSheet("background-color: #ffffff; font-family: 'Segoe UI'; font-size: 12px; color: #333333;")
    # for _key in self.all_reg_dicts[_current_fpga][_current_asic]:
    #     print(f"Register: {_key}")
    #     print(self.all_reg_dicts[_current_fpga][_current_asic][_key])
    _content_table.setRowCount(len(self.reginfo.all_reg_dicts[_current_fpga][_current_asic][_current_reg]) * 2)
    _content_table.setColumnCount(8)
    _content_table.setHorizontalHeaderLabels(["Bit 7", "Bit 6", "Bit 5", "Bit 4", "Bit 3", "Bit 2", "Bit 1", "Bit 0"])
    _table_row_labels = []
    # if 'Global_Analog' in _current_reg:
    #     print(self.reginfo.all_reg_dicts[_current_fpga][_current_asic][_current_reg])
    for i in range(len(self.reginfo.all_reg_dicts[_current_fpga][_current_asic][_current_reg])):
        _table_row_labels.append('Byte')
        _table_row_labels.append(f'{i}')
    _content_table.setVerticalHeaderLabels(_table_row_labels)
    _content_table.horizontalHeader().setDefaultAlignment(Qt.AlignCenter)
    _content_table.verticalHeader().setDefaultAlignment(Qt.AlignCenter)
    _content_table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)

    # set minimum width for each column
    for i in range(8):
        _content_table.setColumnWidth(i, 100)
    for i in range(len(self.reginfo.all_reg_dicts[_current_fpga][_current_asic][_current_reg]) * 2):
        _content_table.setRowHeight(i, 30)
    

    for i in range(len(self.reginfo.all_reg_dicts[_current_fpga][_current_asic][_current_reg]) * 2):
        for j in range(8):
            if i%2 == 1:
                _value = self.reginfo.all_reg_dicts[_current_fpga][_current_asic][_current_reg][i//2] >> (7-j) & 0x1
                # _item = QTableWidgetItem(str(_value))
                # _item.setFlags(_item.flags() | Qt.ItemIsEditable)
                _item_bit_button = BitPushButton(parent.widget(), _value, _current_fpga, _current_asic, _current_reg, i//2, 7-j)
                _item_bit_button.flip_bit_signal.connect(lambda fpga_index_val = _current_fpga, asic_index_val = _current_asic, reg_key_val = _current_reg, byte_index_val = i//2, bit_index_val = 7-j: self.reginfo.flipBit(fpga_index_val, asic_index_val, reg_key_val, byte_index_val, bit_index_val))
                _content_table.setCellWidget(i, j, _item_bit_button)
                # _content_table.setItem(i, j, _item)
            else:
                _value = self.reginfo.all_reg_names[_current_fpga][_current_asic][_current_reg][i//2][j]
                # manual word wrap
                if len(_value) > 21:
                    _value = _value[:10] + '\n' + _value[10:20] + '\n' + _value[20:]
                    _content_table.setRowHeight(i, 70)
                elif len(_value) > 11:
                    _value = _value[:10] + '\n' + _value[10:]
                    _content_table.setRowHeight(i, 50)
                
                _item = QTableWidgetItem(_value)
                _item.setFlags(~Qt.ItemIsEditable)
                _item.setBackground(Qt.gray)
                # change font size
                _item.setFont("font-size: 10px; font-family: 'DejaVu Sans Mono';")
                _content_table.setItem(i, j, _item)
    
    parent.addWidget(_content_table, stretch=3)

def createButtonGroup(self, _current_fpga, _current_asic, label, col_num, parent, button_dict, group):
    _tab_reg_label = QLabel(label)
    _tab_reg_label.setStyleSheet(self.divider_label_format)
    _tab_reg_label.setFixedHeight(20)
    parent.addWidget(_tab_reg_label, alignment=Qt.AlignCenter)
    
    _tab_reg_buttons_layout = QGridLayout()
    parent.addLayout(_tab_reg_buttons_layout)
    col_cnt = 0
    row_cnt = 0
    # Use a modified style for register selection buttons (smaller font)
    reg_button_style = self.common_button_format.replace("font-size: 14px", "font-size: 12px")
    for key in list(self.reginfo.all_reg_dicts[_current_fpga][_current_asic].keys()):
        for _pattern in group:
            if _pattern in key:
                reg_sel_button = QPushButton(key)
                reg_sel_button.setStyleSheet(reg_button_style)
                _tab_reg_buttons_layout.addWidget(reg_sel_button, row_cnt, col_cnt)
                button_dict[key] = reg_sel_button
                col_cnt += 1
                if col_cnt == col_num:
                    col_cnt = 0
                    row_cnt += 1

class DraggablePoints(QObject):
    update_vals_signal = Signal(int, int)

    def __init__(self, key, parent, ax, points, title=None):
        super().__init__()
        self.ax = ax
        self.points = points
        self.cid = points.figure.canvas.mpl_connect('button_press_event', self.on_click)
        self.cid_move = points.figure.canvas.mpl_connect('motion_notify_event', self.on_move)
        self.cid_release = points.figure.canvas.mpl_connect('button_release_event', self.on_release)
        self.selected = None
        self.indicators = []
        self.x_width_factor = ax.get_figure().get_size_inches()[0] / (ax.get_xlim()[1] - ax.get_xlim()[0])
        self.y_height_factor = ax.get_figure().get_size_inches()[1] / (ax.get_ylim()[1] - ax.get_ylim()[0])
        self.ind = None  # Initialize to None for safety
        self.ind_on_click = None
        self.title_default = title
        self.title_not_default = False
        self.mainwindow = parent
        self.key = key

    def update_vals(self, new_val):
        x, y = self.points.get_offsets().T
        y = new_val
        self.points.set_offsets(np.c_[x, y])
        self.points.figure.canvas.draw()

    def on_click(self, event):
        if event.inaxes != self.points.axes:
            return
        if not event.xdata or not event.ydata:
            # print("Event type: ", event)
            return

        x, y = self.points.get_offsets().T
        d = np.hypot((x - event.xdata) * self.x_width_factor, (y - event.ydata) * self.y_height_factor)
        self.selected = np.argmin(d)

    def on_move(self, event):
        if event.inaxes != self.points.axes:
            # show default title if not in the axes
            if self.title_not_default:
                self.ax.set_title(self.title_default)
                self.title_not_default = False
                self.points.figure.canvas.draw()
            return
        
        self.title_not_default = True
        
        x, y = self.points.get_offsets().T
        if self.selected is not None:
            y[self.selected] = round(event.ydata)
            self.points.set_offsets(np.c_[x, y])
        else:
            d = np.hypot((x - event.xdata) * self.x_width_factor, (y - event.ydata) * self.y_height_factor)
            self.ind = np.argmin(d)
        x_title = UniChannelNum2RegKey(self.mainwindow, int(x[self.ind]))
        self.ax.set_title(f'{x_title}: {y[self.ind]}')

        # Remove previous indicators
        for indicator in self.indicators:
            indicator.remove()
        self.indicators = []

        # Draw new indicators
        self.indicators.append(self.ax.axvline(x=x[self.ind], color='gray', linestyle='--'))
        self.indicators.append(self.ax.axhline(y=y[self.ind], color='gray', linestyle='--'))
            
        self.points.figure.canvas.draw()

    def on_release(self, event):
        if self.selected is not None:
            self.update_vals_signal.emit(self.selected, int(self.points.get_offsets()[self.selected][1]))
            self.selected = None

    def disconnect(self):
        self.points.figure.canvas.mpl_disconnect(self.cid)
        self.points.figure.canvas.mpl_disconnect(self.cid_move)
        self.points.figure.canvas.mpl_disconnect(self.cid_release)

def onDraggablePointsUpdate(self, type, ind, new_val, fpga_index, asic_index):
    # print(f"Ind: {ind}, New Val: {new_val} for type: {type}")
    if type == "trim_inv":
        # print(UniChannelNum2RegKey(self, ind))
        # print(self.reginfo.all_reg_dicts[fpga_index][asic_index][UniChannelNum2RegKey(self, ind)].hex())
        old_byte_val = self.reginfo.all_reg_dicts[fpga_index][asic_index][UniChannelNum2RegKey(self, ind)][3]
        new_byte_val = (old_byte_val & 0b00000011) | (new_val << 2)
        self.reginfo.all_reg_dicts[fpga_index][asic_index][UniChannelNum2RegKey(self, ind)][3] = new_byte_val
        # print(self.reginfo.all_reg_dicts[fpga_index][asic_index][UniChannelNum2RegKey(self, ind)].hex())
    elif type == "inputdac":
        # print(UniChannelNum2RegKey(self, ind))
        # print(self.reginfo.all_reg_dicts[fpga_index][asic_index][UniChannelNum2RegKey(self, ind)].hex())
        old_byte_val = self.reginfo.all_reg_dicts[fpga_index][asic_index][UniChannelNum2RegKey(self, ind)][0]
        new_byte_val = (old_byte_val & 0b11000000) | new_val
        self.reginfo.all_reg_dicts[fpga_index][asic_index][UniChannelNum2RegKey(self, ind)][0] = new_byte_val
        # print(self.reginfo.all_reg_dicts[fpga_index][asic_index][UniChannelNum2RegKey(self, ind)].hex())
    elif type == "trim_toa":
        # print(UniChannelNum2RegKey(self, ind))
        # print(self.reginfo.all_reg_dicts[fpga_index][asic_index][UniChannelNum2RegKey(self, ind)].hex())
        old_byte_val = self.reginfo.all_reg_dicts[fpga_index][asic_index][UniChannelNum2RegKey(self, ind)][1]
        new_byte_val = (old_byte_val & 0b00000011) | (new_val << 2)
        self.reginfo.all_reg_dicts[fpga_index][asic_index][UniChannelNum2RegKey(self, ind)][1] = new_byte_val
        # print(self.reginfo.all_reg_dicts[fpga_index][asic_index][UniChannelNum2RegKey(self, ind)].hex())
    elif type == "trim_tot":
        # print(UniChannelNum2RegKey(self, ind))
        # print(self.reginfo.all_reg_dicts[fpga_index][asic_index][UniChannelNum2RegKey(self, ind)].hex())
        old_byte_val = self.reginfo.all_reg_dicts[fpga_index][asic_index][UniChannelNum2RegKey(self, ind)][2]
        new_byte_val = (old_byte_val & 0b00000011) | (new_val << 2)
        self.reginfo.all_reg_dicts[fpga_index][asic_index][UniChannelNum2RegKey(self, ind)][2] = new_byte_val
        # print(self.reginfo.all_reg_dicts[fpga_index][asic_index][UniChannelNum2RegKey(self, ind)].hex())
    self.reginfo.confirmUnsync(fpga_index, asic_index)

def onDraggablePointsLoad(self, type, fpga_index, asic_index):
    data_list = []
    if type == "trim_inv":
        for ind in range(78):
            data_list.append(self.reginfo.all_reg_dicts[fpga_index][asic_index][UniChannelNum2RegKey(self, ind)][3] >> 2)
    elif type == "inputdac":
        for ind in range(78):
            data_list.append(self.reginfo.all_reg_dicts[fpga_index][asic_index][UniChannelNum2RegKey(self, ind)][0] & 0b00111111)
    elif type == "trim_toa":
        for ind in range(78):
            data_list.append(self.reginfo.all_reg_dicts[fpga_index][asic_index][UniChannelNum2RegKey(self, ind)][1] >> 2)
    elif type == "trim_tot":
        for ind in range(78):
            data_list.append(self.reginfo.all_reg_dicts[fpga_index][asic_index][UniChannelNum2RegKey(self, ind)][2] >> 2)
    return data_list

def onHumanModeButtonClicked(mainwindow , _current_fpga, _current_asic):
    updateHumanMode(mainwindow, mainwindow.draggable_plots, _current_fpga, _current_asic)

def setupHumanMode(self, human_layout, human_widget, fpga_index, asic_index):
    for _key in self._human_mode_dplots_properties_dict:
        _dplot_figure = Figure(figsize=(5, 3))
        _dplot = FigureCanvas(_dplot_figure)
        # set minimum size
        _dplot.setMinimumSize(300, 300)
        human_layout.addWidget(_dplot)
        _dplot_ax = _dplot.figure.subplots()
        
        _key_type = _key
        _dplot_ax.set_title(self._human_mode_dplots_properties_dict[_key]["title"])
        _dplot_ax.set_xlabel(self._human_mode_dplots_properties_dict[_key]["xlabel"])
        _dplot_ax.set_ylabel(self._human_mode_dplots_properties_dict[_key]["ylabel"])
        _dplot_ax.set_ylim(self._human_mode_dplots_properties_dict[_key]["ylim"])

        _x_data = np.arange(78)
        _y_data = onDraggablePointsLoad(self, _key_type, fpga_index, asic_index)
        _points = _dplot_ax.scatter(_x_data, _y_data, picker=True, pickradius=50, color=self._human_mode_dplots_properties_dict[_key]["color"])

        _dplot_figure.tight_layout()

        self.draggable_plots[fpga_index][asic_index].append(DraggablePoints(_key, self, _dplot_ax,  _points, self._human_mode_dplots_properties_dict[_key]["title"]))
        self.draggable_plots[fpga_index][asic_index][-1].update_vals_signal.connect(lambda ind, new_val, key=_key_type, fpga_index=fpga_index, asic_index=asic_index: onDraggablePointsUpdate(self, key, ind, new_val, fpga_index, asic_index))

def updateHumanMode(self, draggable_plots, fpga_index, asic_index):
    for _draggable_plot in self.draggable_plots[fpga_index][asic_index]:
        _ax = _draggable_plot.ax
        _y_data = onDraggablePointsLoad(self, _draggable_plot.key, fpga_index, asic_index)
        _x_data = np.arange(78)
        _draggable_plot.update_vals(_y_data)

def setupPedePlot(self, calib_layout, fpga_index, asic_index):
    _pede_figure = Figure(figsize=(5, 3))
    _pede_canvas = FigureCanvas(_pede_figure)
    _pede_canvas.setMinimumSize(300, 300)
    calib_layout.addWidget(_pede_canvas)
    _pede_ax = _pede_canvas.figure.subplots()
    _pede_ax.set_title("Pedestal")
    _pede_ax.set_xlabel("")
    _pede_ax.set_ylabel("Pedestal [ADC]")
    _pede_ax.set_ylim([0, 512])
    
    _x_data = np.zeros(76)
    for _x in range(76):
        _x_data[_x] = (packetlib.uni_chn_to_subblock_list[_x])
    _y_data = np.zeros(76)
    _y_err  = np.zeros(76)

    self.pede_canvas[fpga_index][asic_index] = _pede_canvas
    self.pede_axes[fpga_index][asic_index] = _pede_ax
    self.pede_plots[fpga_index][asic_index] = _pede_ax.errorbar(_x_data, _y_data, yerr=_y_err, fmt='o', color='blue', markersize=3)

def onGetPedestal(self, fpga_index, asic_index):
    # logger.info(f"Get pedestal for FPGA {fpga_index}, ASIC {asic_index}")
    
    # Sample configuration values for the FPGA/ASIC
    top_reg_runLR = [0x0B, 0x0f, 0x40, 0x7f, 0x00, 0x07, 0x05, 0x00]
    top_reg_offLR = [0x08, 0x0f, 0x40, 0x7f, 0x00, 0x07, 0x05, 0x00]
    
    # Reading pedestal values and errors
    val0_list, val0_err = H2GConfig_udp.ReadPedestal(self, fpga_index, asic_index, top_reg_runLR, top_reg_offLR, 10, 3, logger)
    
    # Extract values specific to the ASIC index
    asic_values = []
    asic_err = []
    for i in range(76 * asic_index, 76 * (asic_index + 1)):
        asic_values.append(val0_list[i])
        asic_err.append(val0_err[i])
    
    # Clear the existing plot
    self.pede_axes[fpga_index][asic_index].clear()
    _x_data = np.zeros(76)
    for _x in range(76):
        _x_data[_x] = (packetlib.uni_chn_to_subblock_list[_x])
    self.pede_plots[fpga_index][asic_index] = self.pede_axes[fpga_index][asic_index].errorbar(
        _x_data, asic_values, yerr=asic_err, fmt='o', color='blue', markersize=3
    )
    
    # Redraw the canvas to update the plot with new data
    self.pede_canvas[fpga_index][asic_index].draw()





